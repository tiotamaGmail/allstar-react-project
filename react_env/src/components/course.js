import React from 'react';

class Course extends React.Component{
  constructor(){
    super();

    const courses = [
      {id: 1, img1: 'Hello World', img2: 'You', text1: '이메일인증', text2: '완료', text3: ''},
      {id: 2, img1: 'Installatio', img2: 'You', text1: '본인확인', text2: '가상화폐 입금가능', text3: '입출금한도 1000만원'},
      {id: 3, img1: 'Installatio', img2: 'You', text1: 'OTP인증', text2: '가상화폐 출금가능', text3: '일 출금한도 1000만원'},
      {id: 4, img1: 'Installatio', img2: 'You', text1: '계좌등록', text2: '원화 입출금가능', text3: '일 출금한도 1000만원'},
      {id: 5, img1: 'Installatio', img2: 'You', text1: '신원확인', text2: '출금가능금액 상향', text3: '일 출금한도 1000만원'}
    ];
  }

  render(){
    var {img1, img2, text1, text2, text3} = this.props.courses;
    return(
      <li>
        <a href="#">
          <p><img src={require('../img/' + img1 + '.png')} /></p>
          <p><img src={require('../img/' + img2 + '.png')} /></p>
          <p>{text1}</p>
          <p>{text2}</p>
          <p>{text3}</p>
        </a>
      </li>
    );
  }
}

export default Course;