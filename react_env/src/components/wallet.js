import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { logout } from '../actions/authentication';

const mapStateToProps = (state) => {
  return {
      user:state.session.user.email,
      password:state.session.user.password
  };
};

class Wallet extends React.Component{
	constructor(props) {
		super(props);
    
		this.state = {
      user:this.props.user,
      pw:this.props.password,
      total: 0,
      krwInfo : {},
      check : false,
			walletInfo: []
    };
    
    this.uploadData = this.uploadData.bind(this);
    this.uploadData();
  }
  
  uploadData(){
    let data = {
      'REQ': 'user_wallet_read',
      'email':this.props.user,
      'password':this.props.password
    };
    axios.post('http://api.allstarbit.com/req.php', data)
    .then(response => {
      console.log(response.data);
      if(response.data){
        var krwInfo ={};
        var total = 0;
        var walletInfo = response.data.map(wallet=>{
          total += Number(wallet.balance);
          if(wallet.issue === "KRW"){
            krwInfo = {
              balance: Number(wallet.balance).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
              readOnly : wallet.readOnly,
            }
          }
          return wallet;
        });
        this.setState({
          total : Number(total).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
          krwInfo : krwInfo,
          walletInfo: walletInfo
        });
      }
    })
  }

  checkboxCheck(event)
  {
    if(this.state.check){
      this.setState({check:false})
    }
    else{
      this.setState({check:true})
    }
  }

  render(){
    return(
      <section id="walletCenter">
        <article id="walletArticle1">
          <div id="walletDiv">
            <div className="walletHeader">
              <div>
                <p>총 자산 평가액</p>
                <p><span>{this.state.total}</span>KRW</p>
              </div>
            </div>
            <div className="walletBody">
              <table className="walletBody1T">
                <tbody>
                  <tr>
                    <th>KRW 자산</th>
                    <th>총 보유 금액</th>
                  </tr>
                  <tr>
                    <td>
                      <div></div>
                      <div>
                        <p>대한민국 원</p>
                        <p>KRW</p>
                      </div>
                    </td>
                    <td>{this.state.krwInfo.balance}</td>
                  </tr>
                  <tr className="walletBodyHide">
                    <td colSpan="2">
                      <div>
                        <p>미체결금액</p>
                        <p>0</p>
                      </div>
                      <div>
                        <p>거래 가능 금액</p>
                        <p>0</p>
                      </div>
                      <div>
                        <p><a href="krw_deposit.php">입금</a></p>
                        <p><a href="krw_withdraw.php">출금</a></p>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
              <div className="walletCheckView">
                <p>
                  <input type="checkbox" id="walletBodyCheck" style={{display:'none'}} value="off" onClick={this.checkboxCheck.bind(this)} />
                  <label htmlFor="walletBadyCheck" >
                  {this.state.check?
                  <img src={require("../img/checkbox_on.png")} onClick={this.checkboxCheck.bind(this)} alt="On" />
                  :<img src={require("../img/checkbox_off.png")} onClick={this.checkboxCheck.bind(this)} alt="Off" />}</label>
                  <span>보유 코인만 보기</span>
                </p>
              </div>
              <table className="walletBody2T">
                <tbody>
                    <tr>
                        <th>코인명<img src={require("../img/btn_index.png")} alt="btn-all" /></th>
                        <th>총 보유 수량<img src={require("../img/btn_index.png")} alt="btn-all" /></th>
                    </tr>
                    {
                      this.state.walletInfo.map(walletInfo=>{
                        if((this.state.check && Number(walletInfo.krw_volume))||(this.state.check === false)){
                          return(
                            <tr>
                              <td>
                                  <div></div>
                                  <div>
                                      <p>{walletInfo.issue}</p>
                                      <p>{walletInfo.issue}</p>
                                  </div>
                              </td>
                              <td>
                                  <p>{walletInfo.coin_volume}</p>
                                  <p>≈<span>{walletInfo.krw_volume}</span></p>
                              </td>
                            </tr>
                          );
                        }
                        
                      })
                    }
                </tbody>
              </table>
            </div>
            <div className="walletDepositPopup"></div>
            <div className="walletWithdrawPopup"></div>
          </div>
        </article>
      </section>
    );
  }
}

export default connect(mapStateToProps, null)(Wallet);