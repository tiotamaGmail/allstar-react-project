import React from 'react';
import axios from 'axios';
import Tabs, { Tab } from 'react-awesome-tabs';

class CoinOrder extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      activeTab: 0,
      target: this.props.target,
      source: this.props.source,
      prevClosingPrice: 1,
      sourceWalletInfo : {},
      targetWalletInfo: {},
      totalVolume : 0,
      askVolume : 0,
      askNum : 0,       
      curTradePrice:0,
      initTradePrice:0,
      highVolume : 0,
      isConclusion : true,
      showVolumPopup: false,
      showPricePopup: false,
      askInfoList: [],
      bidInfoList: [],
      transactionList: [],
      conclusionList:[],
      askPopup:false,
      bidPopup:false
    };

    this.timer = 0;
    this.tick = this.tick.bind(this);
    this.uploadData = this.uploadData.bind(this);
    this.overviewUploadData = this.overviewUploadData.bind(this);
    this.walletUploadData = this.walletUploadData.bind(this);
    this.priceHandleClick = this.priceHandleClick.bind(this);
    this.calculator = this.calculator.bind(this);
    this.sendRequest = this.sendRequest.bind(this);
    this.loadTransactionData = this.loadTransactionData.bind(this);
    
    this.overviewUploadData(this.props.source, this.props.target);
    if(this.props.userData.isLogin)
      this.walletUploadData(this.props.source, this.props.target);
  }

  componentWillReceiveProps(nextProps) {
    let nextTradePrice = Number(nextProps.curTradePrice.replace(/[^0-9\.]/g,''));
    if(this.state.curTradePrice !== nextTradePrice)
    {
      this.setState({curTradePrice:nextTradePrice});
    }
    if(this.state.initTradePrice === 0)
    {
      this.setState({initTradePrice:nextTradePrice});
    }
    if(this.state.source !== nextProps.source || this.state.target !== nextProps.target)
    {
      this.setState({
        source : nextProps.source,
        target : nextProps.target,
        initTradePrice : 0
      })
    }
  }

  componentDidMount(){
    this.timer = setInterval(this.tick, 1000);
    this.center();
  }

  componentWillUnmount(){
    clearInterval(this.timer);
  }

  tick(){
    this.uploadData(this.state.source, this.state.target);
    if(!this.state.overviewData){
      this.overviewUploadData(this.state.source, this.state.target);
    }
    if(this.state.activeTab === 2 && this.props.userData.isLogin){
      if(this.state.isConclusion)
        this.loadTransactionData();
      else
        this.loadConclusionData();
    }
    else if(this.props.userData.isLogin)
      this.walletUploadData(this.state.source, this.state.target);
  }

  overviewUploadData(source, target){
    axios.get('http://view.allstarbit.com/view2/overview.php?source='+source)
    .then(response => {
      if(response.data) {
        this.uploadData(source, target);
        var overview = response.data.find(function(element) {
          if(element.target === target && element.source === source){
            return element;
          } 
        });
        this.setState({
          prevClosingPrice : overview.data.prevClosingPrice,
        });
      }
    });
  }

  walletUploadData(source, target){
    let data = {
      'REQ': 'user_wallet_read',
      'email':this.props.userData.user,
      'password':this.props.userData.password
    };
    axios.post('http://api.allstarbit.com/req.php', data)
    .then(response => {
      console.log(response.data);
      var totalVolume = 0;
      if(response.data){
        var sourceWalletInfo = response.data.find(function(element) {
          if(element.issue === source){
            return element;
          } 
        });
        var targetWalletInfo = response.data.find(function(element) {
          if(element.issue === target){
            return element;
          } 
        });
        if(sourceWalletInfo.balance && this.state.activeTab === 0)
        {
          totalVolume = Number(sourceWalletInfo.balance) - Number(sourceWalletInfo.readOnly);
        }
        if(targetWalletInfo.balance && this.state.activeTab === 1)
        {
          totalVolume = Number(targetWalletInfo.balance) - Number(targetWalletInfo.readOnly);
        }
        this.setState({
          sourceWalletInfo: sourceWalletInfo,
          targetWalletInfo: targetWalletInfo,
          totalVolume : totalVolume
        });
      }
    })
  }

  uploadData(source, target){
    axios.get('http://view.allstarbit.com/upbit_streams/calls.php?source='+source+'&target='+target)
    .then(response => {
     // console.log(response);
      var highVolume = 0;
      var askInfoList = response.data.calls.orderbookUnits.map(callInfo=>{
        if(callInfo.askSize > highVolume)
          highVolume = callInfo.askSize;
        if(callInfo.bidSize  > highVolume)
          highVolume = callInfo.bidSize;
        if(this.state.source === 'KRW')
        {
          return {
            askPrice: callInfo.askPrice,
            askSize: callInfo.askSize,
          };
        }else {
          return {
            askPrice: callInfo.askPrice,
            askSize: callInfo.askSize,
            krwPrice: callInfo.askKrwPrice
          };
        }
        
        
      });

      var bidInfoList = response.data.calls.orderbookUnits.map(callInfo=>{
        if(this.state.source === 'KRW')
        {
          return {
            bidPrice: callInfo.bidPrice,
            bidSize: callInfo.bidSize,
          };
        }else {
          return {
            bidPrice: callInfo.bidPrice,
            bidSize: callInfo.bidSize,
            krwPrice: callInfo.bidKrwPrice
          };
        }
      });

      askInfoList.reverse();
      this.setState({
        highVolume: highVolume,
        askInfoList: askInfoList,
        bidInfoList: bidInfoList
      });
    });
  }

  sendRequest(askOrBid){
    let data = {
      'REQ': 'user_wallet_transaction_create',
      'email':this.props.userData.user,
      'password':this.props.userData.password,
      'currency': this.state.source,
      'coin': this.state.target,
      'volume': this.state.askNum,
      'requestValue':this.state.initTradePrice,
      'askOrBid': askOrBid
    };
   
    this.setState({askPopup: false});
    this.setState({bidPopup: false});
    // API REQUEST
    return axios.post('http://api.allstarbit.com/req.php', data).then((response)=>{
      this.initFunction();
      if(this.props.userData.isLogin)
        this.walletUploadData(this.props.source, this.props.target);
      console.log(response);
    }).catch((error)=>{
        // FAILED
        console.log(error);
    });
  }

  loadConclusionData(){
    var rightNow = new Date();
    var finishDate = rightNow.toISOString().slice(0,10);
    rightNow.setDate(rightNow.getDate() - 1);
    var startDate = rightNow.toISOString().slice(0,10);

    var data = {
      'REQ': 'user_wallet_transaction_read_waiting',
      'email':this.props.userData.user,
      'password':this.props.userData.password,
      'startDate': startDate,
      'finishDate': finishDate,
      'pageNumber': 1,
      'pageCount':10,
      'source':this.state.source,
      'target': this.state.target
    };
   
    // API REQUEST
    return axios.post('http://api.allstarbit.com/req.php', data).then((response)=>{
      console.log(response);
      var list = [];
      if(response.data)
      {
        list = response.data.map(data=>{
          return data;
        });
      }
      this.setState({conclusionList:list,transactionList:[]});
    }).catch((error)=>{
        // FAILED
        console.log(error);
    });
  }

  loadTransactionData(){
    var rightNow = new Date();
    var finishDate = rightNow.toISOString().slice(0,10);
    rightNow.setDate(rightNow.getDate() - 1);
    var startDate = rightNow.toISOString().slice(0,10);

    var data = {
      'REQ': 'user_wallet_transaction_read',
      'email':this.props.userData.user,
      'password':this.props.userData.password,
      'startDate': startDate,
      'finishDate': finishDate,
      'pageNumber': 1,
      'pageCount':10
    };
   
    // API REQUEST
    return axios.post('http://api.allstarbit.com/req.php', data).then((response)=>{
      console.log(response);
      if(response.data && response.data.complete)
        this.setState({transactionList:response.data.complete,conclusionList:[]});
    }).catch((error)=>{
        // FAILED
        console.log(error);
    });
  }

  handleTabSwitch(active) {
    var totalVolume = 0;
    if(this.state.sourceWalletInfo.balance && active === 0)
    {
      totalVolume = Number(this.state.sourceWalletInfo.balance) - Number(this.state.sourceWalletInfo.readOnly);
    }
    if(this.state.targetWalletInfo.balance && active === 1)
    {
      totalVolume = Number(this.state.targetWalletInfo.balance) - Number(this.state.targetWalletInfo.readOnly);
    }
    if(active === 2 && this.props.userData.isLogin){
      this.setState({isConclusion : true});
    }
      
    
    this.setState({ activeTab: active, totalVolume:totalVolume });
    this.initFunction();
  }
  
  tradePercentCal(e) {
    var totalVolume = Number(this.state.totalVolume);
    var percent = Number(e.currentTarget.dataset.id);
    var askNum = 0;
    var askVolume = 0;
    if(this.state.initTradePrice !== 0)
    {
      askNum = (totalVolume * percent / 100 /this.state.initTradePrice).toFixed(8);
      askVolume = (askNum * this.state.initTradePrice).toFixed(0);
    }
   
    this.setState({
      showVolumPopup: false,
      askVolume:askVolume,
      askNum: askNum,
    });
  }
  
  tradePriceCal(e){
    console.log(e.currentTarget.dataset.id );
    var percent = Number(e.currentTarget.dataset.id);
    var calValue = this.state.initTradePrice + (this.state.initTradePrice * percent / 100);
    this.setState({
      initTradePrice: calValue,
      showPricePopup : false
    }, this.calculator());
  }

  tradeMinus() {
    var tradePrice = this.state.initTradePrice;
    if(this.state.source === "KRW")
    {
      if(tradePrice > 10000000)
        tradePrice -= 1000;
      else if(tradePrice > 1000000)
      tradePrice -= 500;
      else if(tradePrice > 100000)
        tradePrice -= 50;
      else if(tradePrice > 10000)
        tradePrice -= 10;
      else if(tradePrice > 1500)
        tradePrice -= 50;
      else if(tradePrice > 1000)
        tradePrice -= 10;
      else
        tradePrice -= 1;
    }
    else
      tradePrice -= 0.00000001;
    this.setState({initTradePrice : tradePrice}, this.calculator());
  }

  tradePlus() {
    var tradePrice = this.state.initTradePrice;
    if(this.state.source === "KRW")
    {
      if(tradePrice > 10000000)
        tradePrice += 1000;
      else if(tradePrice > 1000000)
      tradePrice += 500;
      else if(tradePrice > 100000)
        tradePrice += 50;
      else if(tradePrice > 10000)
        tradePrice += 10;
      else if(tradePrice > 1500)
        tradePrice += 50;
      else if(tradePrice > 1000)
        tradePrice += 10;
      else
        tradePrice += 1;
    }
    else
      tradePrice += 0.00000001;
    this.setState({initTradePrice : tradePrice}, this.calculator());
  }

  calculator(){
    var askVolume = 0;
    var askNum = this.state.askNum;
    if(this.state.initTradePrice !== 0 &&  askNum !== 0)
    {
      askVolume = (askNum * this.state.initTradePrice).toFixed(0);
    }
    else
      askVolume = 0;
   
    this.setState({
        askVolume:askVolume,
    });
  }

  priceHandleClick(data) {
    if(data.askPrice)
    {
      this.setState({initTradePrice : Number(data.askPrice.replace(/[^0-9\.]/g,''))}, this.calculator());
    }
    if(data.bidPrice)
    {
      this.setState({initTradePrice : Number(data.bidPrice.replace(/[^0-9\.]/g,''))}, this.calculator());
    }
  }

  initFunction(){
    this.setState({
      initTradePrice : this.state.curTradePrice,
      askVolume:0,
      askNum: 0,
    })
  }

  changeInitTradePride(event) {
    this.setState({ initTradePrice:event.target.value});
    var askVolume = 0;
    var askNum = this.state.askNum;
    var prive = Number(event.target.value);
    if(prive !== 0 &&  askNum !== 0)
    {
      askVolume = (askNum * prive).toFixed(0);
    }
    else
      askVolume = 0;
   
    this.setState({
        askVolume:askVolume,
    });
  }

  changeNum(event) {
    this.setState({ askNum:event.target.value});
    var askVolume = 0;
    var askNum = Number(event.target.value);
    var prive = this.state.initTradePrice;
    if(prive !== 0 &&  askNum !== 0)
    {
      askVolume = (askNum * prive).toFixed(0);
    }
    else{
      askVolume = 0;
    }
      
    this.setState({
        askVolume:askVolume,
    });
  }

  cancelConclusion(data) {
    var rightNow = new Date();
    var finishDate = rightNow.toISOString().slice(0,10);
    rightNow.setDate(rightNow.getDate() - 1);
    var startDate = rightNow.toISOString().slice(0,10);

    var data = {
      'REQ': 'user_wallet_transaction_delete',
      'email':this.props.userData.user,
      'password':this.props.userData.password,
      'currency': this.state.source,
      'coin' : this.state.target,
      'askOrBid': data.askOrBid,
      'idx': data.idx
    };
   
    // API REQUEST
    return axios.post('http://api.allstarbit.com/req.php', data).then((response)=>{
      console.log(response);
    }).catch((error)=>{
        // FAILED
        console.log(error);
    });
  }

  showPopup(){
    this.setState({showPricePopup: true});
    for (let i = 1; i <= 665; i++) {
      setTimeout(() => (this.node.scrollTop = i), i * 2);
    }
  }

  center(){
    for (let i = 1; i <= 95; i++) {
      setTimeout(() => (this.fixedTableBodyList.scrollTop = i), i * 2);
      console.log(i);
    }
  }
  

  render(){
    const tradeStyle = {border:"1px solid #fff"};
    let {prevClosingPrice, highVolume, curTradePrice} = this.state;
    const showStyle1 = {
      height: '175px',
    };
    const showStyle2 = {
      height: '175px',
      "overflow-y" : 'scroll'
    };
    const hideStyle = {
      height: '35px',
      "overflow":"hidden"
    };

    const makeChoice = () =>{
      var rows = [];
      for(var temp=100; temp >= -100; temp-=5)
      {
        rows.push(<li onClick={this.tradePriceCal.bind(this)} data-id={temp}> {temp}% </li>);
      }
      return rows;
    }

    return(
      <div className="tradeDetailBody0">
        <ul className="tradeDetailDiv0 clear">
          <li className="tradeDetailLeft0">
            <div  ref={fixedTableBodyList => this.fixedTableBodyList = fixedTableBodyList}>
              <table className="tradeDetailLeftTable"  >
                <tbody>
                  {this.state.askInfoList.map(data=>{
                    let tradePriceNum = Number(data.askPrice.replace(/[^0-9\.]/g,''))
                    let priceRate = Number(((tradePriceNum * 100) / prevClosingPrice) - 100).toFixed(2); 
                    let volumeRate = Number(((data.askSize * 100) / highVolume));
                    
                    return (
                      <tr onClick={this.priceHandleClick.bind(this, data)}>
                        <td className="tradeDetailRed" style={(tradePriceNum === curTradePrice)?tradeStyle:null}>{data.askPrice} <span>{(this.state.source === 'KRW')?priceRate+'%':data.krwPrice}</span></td>
                        <td><p className="tradeDetailBred" style={{width:volumeRate+'%'}}>&nbsp;{data.askSize}</p></td>
                      </tr>
                    )}
                  )} 
                  {this.state.bidInfoList.map(data=>{
                    let tradePriceNum = Number(data.bidPrice.replace(/[^0-9\.]/g,''))
                    let priceRate = Number(((tradePriceNum * 100) / prevClosingPrice) - 100).toFixed(2); 
                    let volumeRate = Number(((data.bidSize * 100) / highVolume));
 
                    return (
                      <tr onClick={this.priceHandleClick.bind(this, data)}>
                        <td className="tradeDetailBlue" style={(tradePriceNum === curTradePrice)?tradeStyle:null}>{data.bidPrice} <span>{(this.state.source === 'KRW')?priceRate+'%':data.krwPrice}</span></td>
                        <td><p className="tradeDetailBblue" style={{width:volumeRate+'%'}}>&nbsp;{data.bidSize}</p></td>
                      </tr>
                    )}
                  )}
                </tbody>
              </table>
            </div>
          </li>
          <li className="tradeDetailRight0">
            <Tabs	active={ this.state.activeTab }	onTabSwitch={ this.handleTabSwitch.bind(this) }>
              <Tab title="매수">
                <div className="detailRight0Center">
                  <p className="detailRight0Available clear">
                    <span>주문가능</span>
                    {
                      (this.props.userData.isLogin)?
                      <span>{this.state.totalVolume} {this.state.sourceWalletInfo.issue}</span>:
                      <span>로그인이 필요합니다.</span>
                    }
                  </p>
                  <div className="detailRight0Amount clear">
                    <input type="number"  value={this.state.askNum} onChange={this.changeNum.bind(this)} style={{position:'relative',top:'-5px',width:'calc(70% - 15px)',height:'35px',padding:'0 5px',marginRight:'5px',background:'#fff',border:'1px solid rgba(0,0,0,1)',color:'#000',float:'left',textAlign:'right'}}/>
                    <p onClick={() => this.setState({showVolumPopup: true})}>가능</p>
                    {(this.state.showVolumPopup)?
                    <ul>
                      <li onClick={this.tradePercentCal.bind(this)} data-id="100">100% 최대</li>
                      <li onClick={this.tradePercentCal.bind(this)} data-id="75">75%</li>
                      <li onClick={this.tradePercentCal.bind(this)} data-id="50">50%</li>
                      <li onClick={this.tradePercentCal.bind(this)} data-id="25">25%</li>
                      <li onClick={this.tradePercentCal.bind(this)} data-id="10">10%</li>
                    </ul>:null}
                  </div>
                  <div className="clear detailRight0Calc">
                    <input type="number" value={this.state.initTradePrice} onChange={this.changeInitTradePride.bind(this)} style={{position:'relative',top:'-5px',width:'calc(70% - 15px)',height:'35px',padding:'0 5px',marginTop:'10px',marginRight:'5px',background:'#fff',border:'1px solid rgba(0,0,0,1)',color:'#000',float:'left',textAlign:'right'}}/>
                    <p className="minusBtn" onClick={this.tradeMinus.bind(this)}>-</p>
                    <p className="plusBtn" onClick={this.tradePlus.bind(this)}>+</p>
                  </div>
                  <div>
                    <ul className="detailRight0pricePercent clear" align="center" ref={node => this.node = node} style={this.state.showPricePopup? showStyle2: hideStyle}>
                      <li align="center" className="test clear" onClick={this.showPopup.bind(this)} >현재가 대비 ▼</li>
                      {
                        makeChoice()
                      }
                    </ul>
                  </div>
                  <div className="detailRight0sum clear">
                    <span>주문총액</span><span>KRW</span><span>{this.state.askVolume}</span>
                  </div>
                  <div value="매수" className="detailRight0Reset" onClick={this.initFunction.bind(this)} >초기화</div>
                  <div value="매수" className="detailRight0Submit" onClick={() => this.setState({askPopup: true})}>매수</div>
                  <p className="detailRight0Align clear"><span>최소주문금액</span><span>1000&nbsp;KRW</span></p>
                  <p className="detailRight0Align clear"><span>수수료(부가세 포함)</span><span>0.05%</span></p>
                  <p className="detailRight0QuoteOrder" style={{display:'none'}}>호가주문</p>
                </div>
              </Tab>
              <Tab title="매도">
                <div className="detailRight1Center">
                  <p className="detailRight1Available clear">
                    <span>주문가능</span>
                    {
                      (this.props.userData.isLogin)?
                      <span>{this.state.totalVolume} {this.state.targetWalletInfo.issue}</span>:
                      <span>로그인이 필요합니다.</span>
                    }
                  </p>
                  <div className="detailRight1Amount clear">
                    <input type="number" value={this.state.askNum} onChange={this.changeNum.bind(this)} style={{position:'relative',top:'-5px',width:'calc(70% - 15px)',height:'35px',padding:'0 5px',marginRight:'5px',background:'#fff',border:'1px solid rgba(0,0,0,1)',color:'#000',float:'left',textAlign:'right'}}/>
                    <p onClick={() => this.setState({showVolumPopup: true})}>가능</p>
                    {(this.state.showVolumPopup)?   
                    <ul>
                      <li onClick={this.tradePercentCal.bind(this)} data-id="100">100% 최대</li>
                      <li onClick={this.tradePercentCal.bind(this)} data-id="75">75%</li>
                      <li onClick={this.tradePercentCal.bind(this)} data-id="50">50%</li>
                      <li onClick={this.tradePercentCal.bind(this)} data-id="25">25%</li>
                      <li onClick={this.tradePercentCal.bind(this)} data-id="10">10%</li>
                    </ul>:null}
                  </div>
                  <div className="clear detailRight1Calc">
                    <input type="number" value={this.state.initTradePrice} onChange={this.changeInitTradePride.bind(this)} style={{position:'relative',top:'-5px',width:'calc(70% - 15px)',height:'35px',padding:'0 5px',marginTop:'10px',marginRight:'5px',background:'#fff',border:'1px solid rgba(0,0,0,1)',color:'#000',float:'left',textAlign:'right'}}/>
                    <p className="minusBtnSell" onClick={this.tradeMinus.bind(this)}>-</p>
                    <p className="plusBtnSell" onClick={this.tradePlus.bind(this)}>+</p>
                  </div>
                  <div>
                    <ul className="detailRight0pricePercent clear" align="center" style={this.state.showPricePopup? showStyle2: hideStyle}>
                      <li align="center" className="clear" onClick={() => this.setState({showPricePopup: true})}>현재가 대비 ▼</li>
                      {
                        makeChoice()
                      }
                    </ul>
                  </div>
                  <div className="detailRight1sum clear">
                    <span>주문총액</span><span>KRW</span><span>{this.state.askVolume}</span>
                  </div>
                  <div value="매수" className="detailRight1Reset" onClick={this.initFunction.bind(this)}>초기화</div>
                  <div value="매수" className="detailRight1Submit" onClick={() => this.setState({bidPopup: true})}>매도</div>
                  <p className="detailRight1Align clear"><span>최소주문금액</span><span>1000&nbsp;KRW</span></p>
                  <p className="detailRight1Align clear"><span>수수료(부가세 포함)</span><span>0.05%</span></p>
                </div>
              </Tab>
              <Tab title="거래내역">
                <div className="detailRight2Center">
                {
                  (this.props.userData.isLogin)?
                  <table>
                    <tbody>
                      <tr>
                        <th colSpan="6">
                          <input type="radio" name="example" id="detailRight2Conclusion" checked={this.state.isConclusion} onClick={() => this.setState({isConclusion: true})}/>
                          <label htmlFor="detailRight2Conclusion">체결</label>
                          &nbsp;
                          <input type="radio" name="example" id="detailRight2Nonconclusion" checked={!this.state.isConclusion} onClick={() => this.setState({isConclusion: false})}/>
                          <label htmlFor="detailRight2NonConclusion">미체결</label>
                        </th>
                      </tr>
                      {
                        this.state.isConclusion?
                        <tr>
                          <td>체결시간</td>
                          <td>구분</td>
                          <td>주문가격</td>
                          <td>체결량</td>
                        </tr>:
                        <tr>
                          <td>체결시간</td>
                          <td>구분</td>
                          <td>주문가격</td>
                          <td>체결량</td>
                          <td>취소</td>
                        </tr>
                      }
                      {
                        this.state.isConclusion?
                        this.state.transactionList.map(data=>{
                          return(
                            <tr>
                              <td>{data.procDate.substring(5,16)}</td>
                              <td><font style={(data.askOrBid === "1")?{color:"#d24f45"}:{color:"#297eff"}}>{(data.askOrBid === "1")?"매수":"매도"}</font></td>
                              <td>{data.requestValue}</td>
                              <td>{data.volume}</td>
                            </tr>
                          )
                        }):
                        this.state.conclusionList.map(data=>{
                          return(
                            <tr>
                              <td>{data.regDate.substring(5,16)}</td>
                              <td><font style={(data.askOrBid==="1")?{color:"#d24f45"}:{color:"#297eff"}}>{(data.askOrBid === "1")?"매수":"매도"}</font></td>
                              <td>{data.requestValue}</td>
                              <td>{data.volume}</td>
                              <td className="detailRight2Cancel"><p onClick={this.cancelConclusion.bind(this,data)}>취소</p></td>
                            </tr>
                          )
                        })
                      }
                    </tbody>
                  </table>:
                  <div style={{textAlign:"center"}}>
                    <br></br>
                    로그인이 필요합니다.
                  </div>
                }
                </div>
              </Tab>
            </Tabs>
          </li>
        </ul>
        <div class="coinorderPopup" style={(this.state.askPopup)?{display:"block"}:{display:"none"}}>
          <div>
            <p>매수주문 확인</p>
            <div class="coinorderPopupName">비트코인캐시({this.state.target}/{this.state.source})</div>
            <ul>
              <li>
                <p>주문구분</p>
                <p>
                  <span>지정가</span>
                  <span>매수</span>
                </p>
              </li>
              <li>
                <p>주문수량</p>
                <p>
                  <span>{this.state.askNum}</span>
                  <span>{this.state.target}</span>
                </p>
              </li>
              <li>
                <p>주문가격</p>
                <p>
                  <span>{this.state.askVolume}</span>
                  <span>KRW</span>
                </p>
              </li>
            </ul>
            <div class="coinorderPopupButton">
              <div onClick={() => this.setState({askPopup: false})}>취소</div>
              <div onClick={this.sendRequest.bind(this,"1")}>매수확인</div>
            </div>
          </div>
        </div>
        <div class="coinorderPopup" style={(this.state.bidPopup)?{display:"block"}:{display:"none"}}>
          <div>
            <p>매도주문 확인</p>
            <div class="coinorderPopupName">비트코인캐시({this.state.target}/{this.state.source})</div>
            <ul>
              <li>
                <p>주문구분</p>
                <p>
                  <span>지정가</span>
                  <span>매도</span>
                </p>
              </li>
              <li>
                <p>주문수량</p>
                <p>
                  <span>{this.state.askNum}</span>
                  <span>{this.state.target}</span>
                </p>
              </li>
              <li>
                <p>주문가격</p>
                <p>
                  <span>{this.state.askVolume}</span>
                  <span>KRW</span>
                </p>
              </li>
            </ul>
            <div class="coinorderPopupButton">
              <div onClick={() => this.setState({bidPopup: false})}>취소</div>
              <div onClick={this.sendRequest.bind(this,"2")}>매도확인</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default CoinOrder;