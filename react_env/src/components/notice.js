import React from 'react';
import Trandtabs from './noticetab/noticetab';
import axios from 'axios';

class Notice extends React.Component{
  constructor(props){
    super(props);
    this.state = {active: 'aTab'};
    axios.get('http://13.209.3.126/news_streams/')
  .then(response => {
    console.log(response)
  })
  }

  
  render(){
    
    return(
      <section id="noticeCenter">
        <article id="article1">
          <div className="noticeTitle">올스타빗 공지사항</div>
          <Trandtabs active={this.state.active} onChange={active => this.setState({active})} >
            <div key="aTab">전체</div>
            <div key="bTab">공지</div>
            <div key="cTab">이벤트</div>
            <div key="dTab">코인뉴스</div>
            <div key="eTab">상장</div>
          </Trandtabs>

          <div className="tradeContent">
            <table className="noticeList">
              <tr>
                <td>
                  <p>
                    <span>[공지]</span>
                    <span>공지사항 1</span>
                    <span>></span>
                  </p>
                  <p>
                    test
                  </p>
                </td>
              </tr>
              <tr>
                <td>
                  <p>
                    <span>[공지]</span>
                    <span>공지사항 2</span>
                    <span>></span>
                  </p>
                  <p>
                    test
                  </p>
                </td>
              </tr>
            </table>
          </div>
        </article>
      </section>
    );
  }
}

//<div className="tradeContent">{content[this.state.active]}</div>

export default Notice;