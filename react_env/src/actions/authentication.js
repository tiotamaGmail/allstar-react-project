import axios from 'axios';
import { sessionService } from 'redux-react-session';

import {
  AUTH_LOGIN,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGIN_FAILURE,
  AUTH_LOGOUT,
  AUTH_REGISTER,
  AUTH_REGISTER_SUCCESS,
  AUTH_REGISTER_FAILURE,
  AUTH_SEND_EMAIL,
  AUTH_RECIEVE_EMAIL,
  AUTH_EMAIL_SUCCESS,
  AUTH_EMAIL_FAILURE,
  AUTH_FAVORITE,
  AUTH_FAVORITE_SUCCESS,
  AUTH_FAVORITE_FAILURE,
  COINLIST_LOAD,
  COINLIST_SET_INDEX
} from './ActionTypes';


/*============================================================================
    authentication
==============================================================================*/
function base_convert(number, frombase, tobase) {
  return parseInt(number + '', frombase | 0)
    .toString(tobase | 0);
}

export function pj_code(str) {
  let enc='';
  let len=str.length;
  while(len--) {
    enc+=base_convert(str.substr(len,1).charCodeAt(0),10,36)+'-';
  }
  return enc;
}

/* LOGIN */
export function loginRequest(username, password) {
  return (dispatch) => {
    // Inform Login API is starting
    dispatch(login());
    let code = pj_code(password);
    let data = {
      'REQ': 'user_read',
      'email':username,
      'password':code
    };
   
    // API REQUEST
    return axios.post('http://api.allstarbit.com/req.php', data).then((response)=>{
      // SUCCEED
      if(response.data.result.email) {
      	console.log('response: ', response);
				dispatch(loginSuccess(username,pj_code(password)));
			} else {
				dispatch(loginFailure());
			}
    }).catch((error)=>{
        // FAILED
        dispatch(loginFailure());
    });
  };
}

export function login() {
  return {
      type: AUTH_LOGIN
  };
}

export function loginSuccess(username, password) {
  return {
      type: AUTH_LOGIN_SUCCESS,
      username,
      password
  };
}

export function loginFailure() {
  return {
      type: AUTH_LOGIN_FAILURE
  };
}

export function logout() {
  return {
      type: AUTH_LOGOUT
  };
}

export function logoutRequest() {
	return () => {
		return Promise.all(
			[sessionService.deleteSession(), sessionService.deleteUser()]
		).then(() => {
			console.log('logout success');
		}).catch((err) => {
			console.log('logout failed: ', err);
		});
	}
}

/* REGISTER */
export function registerRequest(username, password, confirm) {
  return (dispatch) => {
    // Inform Register API is starting
    dispatch(register());
    let code1 = pj_code(password);
    let code2 = pj_code(confirm);
    
    let data = {
      'REQ': 'user_create',
      'email':username,
      'password':code1,
      'passwordConfirm':code2,
      'phone':'01011112222',
      'sangsangCode':""
    };
    return axios.post('http://api.allstarbit.com/req.php', data).then((response) => {
      if(response.data.result === 1)
        dispatch(registerSuccess());
      else
        dispatch(registerFailure(response.data.result));// add Error Response
    }).catch((error) => {
     // console.log(error.response);
    });
  };
}

export function register() {
  return {
      type: AUTH_REGISTER
  };
}

export function registerSuccess() {
  return {
      type: AUTH_REGISTER_SUCCESS,
  };
}

export function registerFailure(error) {
  return {
      type: AUTH_REGISTER_FAILURE,
      error
  };
}

export function sendSingupEmailRequest(username) {
  return (dispatch) => {
    // Inform sendEmail API is starting
    let data = {
      'REQ': 'sendSignUpEmail',
      'email':username
     };
    axios.post('http://api.allstarbit.com/req.php', data)
    .then(response => {
     // console.log(response.data);
    });
  };
}

export function recieveSingupEmailRequest(username, password, rpc) {
  return (dispatch) => {
    // Inform recieve Email API is starting
    let data = {
      'REQ': 'receiveSignUpEmail',
      'email':username,
      'rpc':rpc,
      'password':pj_code(password)
     };
    return axios.post('http://api.allstarbit.com/req.php', data)
    .then(response => {
      if(response.data === 2001)
        dispatch(signupEmailSuccess()); 
      else
        dispatch(signupEmailFailure()); 
      //console.log(response.data);
    });
  };
}

export function signupEmailSuccess() {
  return {
    type: AUTH_EMAIL_SUCCESS,
  };
}

export function signupEmailFailure() {
  return {
    type: AUTH_EMAIL_FAILURE,
  };
}

export function favoriteRequest(username,password,issue) {
  return (dispatch) => {
    // Inform Login API is starting
    //dispatch(favorite());
    let code = pj_code(password);
    let data = {
      'REQ': 'user_wallet_favorite',
      'email ':username,
      'password':code,
      'issue':issue
    };
    // API REQUEST
    return axios.post('http://api.allstarbit.com/req.php', data).then((response)=>{
      // SUCCEED
      console.log(response)
      dispatch(favoriteSuccess());
    }).catch((error)=>{
      // FAILED
      dispatch(favoriteError());
    });
  };
}

export function favoriteSuccess() {
  return {
      type: AUTH_FAVORITE_SUCCESS,
  };
}

export function favoriteError() {
  return {
      type: AUTH_FAVORITE_FAILURE,
  };
}

export function coinListRequest(source, order, sort) {
  return (dispatch) => {
    axios.get('http://view.allstarbit.com/view2/overview.php?source='+source+'&order='+order+'&sort='+sort)
    .then((response) => {
      let idx = 0;
      var coinList = response.data.map(bitcoin => {
        if(bitcoin.data){
          return {
            id:idx++,
            target: bitcoin.target,
            source: bitcoin.source,
          }
        }
      });
      dispatch(loadCoinListSuccess(coinList));
      return true;
    });
  };
}

export function coinListSearch(source, order, sort) {
  return (dispatch) => {
    axios.get('http://view.allstarbit.com/view2/overview.php?source='+source+'&order='+order+'&sort='+sort)
    .then((response) => {
      let idx = 0;
      var coinList = response.data.map(bitcoin => {
        if(bitcoin.data){
          return {
            id:idx++,
            target: bitcoin.target,
            source: bitcoin.source,
          }
        }
      });
      dispatch(loadCoinListSuccess(coinList));
      return true;
    });
  };
}

export function setCoinIndex(id) {
  return (dispatch) => {
    dispatch(setIndexSuccess(id));
  };
}

export function loadCoinListSuccess(coinList) {
  return {
      type: COINLIST_LOAD,
      coinList
  };
}
export function setIndexSuccess(id) {
  return {
      type: COINLIST_SET_INDEX,
      id
  };
}
