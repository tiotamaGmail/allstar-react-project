import React from 'react';
import {Link} from "react-router-dom";

class Exchangemain extends React.Component{
  render(){
    return(
        <section class="main_wrap" style={{width:'100%',color:'#fff',textAlign:'center',background:'#202126'}}>
            <article>
            <div class="top_banner rel">
                <div id="main_banner">
                    <Link to=""><img src={('http://allstarbit.com/m_exchange/img/banner11.jpg?v=2')} style={{width:'100%'}}/></Link>
                </div>
                <div class="container" style={{padding:'100px 10px 220px 10px'}}>
                    <div class="justify-content-center align-items-center" style={{padding:'0',margin:'0'}}>
                        <div style={{width:'100%',fontSize:'23px',fontWeight:'400',textAlign:'center',lineHeight: '1.5'}}>
                            <span style={{fontSize:'40px',color:'#ffce00'}}>NO.1 보안시스템</span>
                            <br/><br/> 암호화폐 거래소<br/> 올스타빗 거래소 오픈
                        </div>
                        <div class="col-12 jh" style={{fontSize:'11px', fontWeight:'bold', paddingTop:'20px', paddingBottom:'20px'}}>
                            NO.1 SECURITY SYSTEM, ALLSTARBIT EXCAHNGE OPEN
                        </div>
                        <div style={{width:'fit-content',height:'58px', margin:'0 auto'}}>
                            <div class="exchange_btn1" style={{border:'1px solid #ffce00', width:'130px', height:'48px', lineHeight:'48px',fontWeight:'normal', color:'#ffce00',margin:'5px',fontSize:'12px',float:'left',borderRadius:'5px',cursor:'pointer'}}>
                                <Link to="trade" style={{color:'rgb(255, 206, 0)'}}>
                                거래소 둘러보기
                                </Link>
                            </div>
                            <div class="exchange_btn1" style={{border:'1px solid #ffce00', width:'130px', height:'48px',lineHeight:'48px',fontWeight:'normal',color:'#ffce00', margin:'5px',fontSize:'12px',float:'left',borderRadius:'5px'}}>
                                거래소 시작하기
                            </div>
                            <div style={{width:'fit-content',height:'58px',margin:'0 auto'}}>
                                <div style={{width:'272px',height:'48px',margin:'10px 5px 5px',background: 'rgb(255,206,0)',border:'1px solid rgb(255,206,0)', borderRadius:'5px',fontSize:'12px',fontWeight:'normal',color:'#000', lineHeight:'48px',float:'left',cursor:'pointer'}}>
                                    모의테스트 자세히보기 >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="main_modal abs" style={{display:'none',padding:'10% 0 15% 0', borderTop:'1px solid #fff',borderBottom:'1px solid #fff'}}>
                    <p style={{fontSize:'25px'}}>모의 테스트 가이드</p><br/>
                    <div class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-timeout="0">
                        <div class="cycle-prev"></div>
                        <div class="cycle-next"></div>
                        <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/guide_1.jpg')} style={{width:'100%'}}/>
                        <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/guide_2.jpg')} style={{width:'100%'}}/>
                        <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/guide_3.jpg')} style={{width:'100%'}}/>
                        <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/guide_4.jpg')} style={{width:'100%'}}/>
                    </div>
                    <div class="close_btn abs" style={{padding:'3%',borderTop:'2px solid #fff',borderBottom:'2px solid #fff',left:'0',right:'0',bottom:'0'}}>x 닫기</div>
                </div>
            </div>

            <div class="top_menu" style={{display:'none'}}>
                <div class="container" style={{padding:'0px'}}>
                    <div id="web_links" style={{padding:'0',margin:'0'}}>
                        <div class="col-12" style={{fontSize:'15px',fontWeight: 'bold',color:'black'}}>
                            최대 최다 알트 코인 모의 테스트 런칭
                        </div>
                    </div>
                </div>
            </div>

            <div class="main_conts">
                <div class="col-12" style={{margin:'0', padding:'0'}}>
                    <img src={('http://allstarbit.com/m_exchange/img/deep.png')} style={{width:'100%'}} />
                </div>
                <div class="container" style={{width:'calc(100% - 30px)',margin:'0 auto', paddingRight:'15px',paddingLeft:'15px'}}>
                    <div class="justify-content-center pt-5 pb-3" style={{color:'#ffce00',fontSize:'12px',fontWeight:'1e3',padding:'3rem 0 1rem', margin:'0'}}>
                        <div class="col-12 pb-2" style={{paddingBottom:'.5rem'}}>ALLSTARBIT OVERVIEW</div>
                        <div style={{width:'30px',borderBottom:'2px solid #ffce00',margin:'0 auto'}}></div>
                    </div>

                    <div class="col-12 pt-5" style={{paddingTop:'3rem'}}>
                        <p style={{fontWeight: '1e3',fontSize:'25px',color:'rgb(24, 207, 145)',marginBottom:'1rem'}}>
                            NO.1 SECURITY SYSTEM<br/>
                        </p>
                        <div class="pb-5" style={{fontSize:'10px',fontWeight: 'normal',paddingBottom:'3rem',lineHeight:'1.5'}}>올스타빗 거래소에서는 최고의 서비스와 최적화된 시스템으로 비트코인 및 이더리움 등 다양한 암호화폐를 거래할 수 있습니다.</div>
                        <div style={{fontSize:'16px',color:'#ffce00',fontWeight:'bold',lineHeight:'1.5'}}>4차산업의 혁명, 미래금융의 가치 암호화폐<br/>거래는 올스타빗에서부터!</div>
                    </div>
                    <div class="pt-5 pb-5" style={{height:'fit-content',display:'block',padding:'3rem 0 0',margin:'0'}}>
                        <div class="col-6 p-1" style={{position:'relative',width:'100%',maxWidth:'calc(50% - .5rem)',padding:'.25rem .25rem .5rem',flex:'0 0 50%',float:'left'}}>
                            <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/img_1.png')} style={{width:'100%',maxWidth:'165px'}}/>
                        </div>
                        <div class="col-6 p-1" style={{position:'relative',width:'100%',maxWidth:'calc(50% - .5rem)',padding:'.25rem .25rem .5rem',flex:'0 0 50%',float:'left'}}>
                            <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/img_2.png')} style={{width:'100%',maxWidth:'165px'}}/>
                        </div>
                        <div class="col-6 p-1" style={{position:'relative',width:'100%',maxWidth:'calc(50% - .5rem)',padding:'0 .25rem .25rem',flex:'0 0 50%',float:'left'}}>
                            <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/img_3.png')} style={{width:'100%',maxWidth:'165px'}}/>
                        </div>
                        <div class="col-6 p-1" style={{position:'relative',width:'100%',maxWidth:'calc(50% - .5rem)',padding:'0 0.25rem 0.25rem',flex:'0 0 50%',float:'left'}}>
                            <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/img_4.png')} style={{width:'100%',maxWidth:'165px'}}/>
                        </div>
                    </div>
                    
                    <div class="exchangeMainChapter" style={{clear:'both'}}>
                        <p style={{fontSize:'20px', color:'#fff',fontWeight:'bold',lineHeight:'1.5'}}>암호화폐 거래소 <span style={{fontWeight:'1e3'}}>올스타빗</span></p>
                        <p style={{fontSize:'16px',fontWeight:'bold',color:'rgb(255, 206, 0)',lineHeight:'1.5'}}>"딥러닝을 이용해 보안시스템을 완벽하게 구축"</p>
                        <div class="exchangeMainChapter0">
                            <div style={{width:'150px',height:'150px',borderRadius:'50%',background:'rgba(255,255,255,0.5)',padding:'0',margin:'2rem auto'}}></div>
                            <p style={{fontSize:'13px',fontWeight:'normal',color:'#fff',lineHeight:'1.5'}}>
                                국내 최대 최다 암호화폐 거래소인 올스타빗(AllStarBit)이 5월 On-Offline 동시오픈!
                                <br/><br/>
                                차세대 딥러닝 인공지능 거래 시스템을 도입하여 보안을 한층 더 강화하였습니다.
                                <br/><br/>
                                올스타빗(AllStarBit)은 블록체인과 관련한 국내외 핵심 개발진들이 참여하여 기존의 거래소들보다 거래환경을 대폭 개선하였습니다.
                            </p>
                        </div>
                        <div class="exchangeMainChapter0">
                            <div style={{width:'150px',height:'150px',borderRadius:'50%',background:'rgba(255,255,255,0.5)',padding:'0',margin:'2rem auto'}}></div>
                            <p style={{fontSize:'13px',fontWeight:'normal',color:'#fff',lineHeight:'1.5'}}>
                                암호화폐 상담과 거래의 편의성을 위해 올스타빗(AllStarBit) 오프라인거래소 또한 오픈!
                                <br/><br/>
                                온라인거래소 런칭과 더불어 비트코인, 이더리움 등 대표코인과 함께 다양한 알트코인을 선보일 예정입니다.
                                <br/><br/>
                                또한, 이더리움 스마트컨트렉트 서비스를 적용하여 거래소와 유저들이 함께 성장할 수 있는 거래소 플랫폼을 구축해 갈 것입니다.
                            </p>
                        </div>
                    </div>

                    <div style={{width:'100%',borderBottom:'2px solid #fff',clear:'both',paddingTop:'3rem'}}></div>

                    <div class="pb-5 pt-2" style={{textAlign:'left',fontSize:'20px',fontWeight:'1e3',color:'rgb(255, 206, 0)',paddingTop:'.5rem',paddingBottom:'3rem',lineHeight:'1.5'}}>
                        ALLSTARBIT INTRODUCE
                        <p style={{fontSize:'13px',fontWeight:'normal',textAlign: 'justify',color:'#fff',paddingTop:'10px',lineHeight:'1.5',marginBottom:'1rem'}}>
                        올스타빗 거래소는 전세계 고객이 24시간, 365일 문제 없도록 동시거래를 하실 수 있게 설계된 서버와 해킹방지를 위한 암호화된 콜드지갑 및 핫지갑의 사용으로 
                        <span style={{color:'rgb(255, 206, 0)'}}> 대한민국 NO.1 보안시스템</span>을 갖추고 있는 거래소입니다.
                        </p>
                        <div style={{width:'100%',border:'1px solid #fff'}}></div>
                    </div>

                    <div>
                        <p style={{marginBottom:'3rem',fontSize:'13px',fontWeight:'normal',textAlign:'justify',color:'#fff',lineHeight:'1.5'}}>
                            모든 고객님의 개인신상정보, 로그인정보,거래정보, 입출금정보를 갖고 있는 계정에 대해 제3자에 의한 피해방지를 하기 위하여 
                            <span style={{color:'rgb(255, 206, 0)'}}>실시간 모니터링으로 안심거래</span>를 하실 수 있는 거래소입니다.
                            <br/><br/>
                            코인동향과 정보를 한눈에 확인할 수 있으며 <span style={{color:'rgb(255, 206, 0)'}}>실시간으로 반영되는 코인의 등락률</span>을 통해 분석하여 투자가 이루어지도록 합니다.
                            <br/><br/>
                            올스타빗에서 <span style={{color:'rgb(255, 206, 0)'}}>최고의 서비스와 최적화된 시스템</span>으로 비트코인 및 이더리움 등
                            다양하고 새롭게 상장되는 암호화 화폐들도 빠르게 거래할 수 있도록 온라인 및 오프라인 등 모든 면에서 앞서가도록 노력하겠습니다.
                        </p>
                    </div>

                    <div class="chapter_01" style={{marginTop:'15%'}}>
                        <div class="jh" style={{color:'#ffce00',fontSize:'17px',letterSpacing:'15px',marginBottom:'50px',fontWeight:'400'}}>
                            <p>
                                CHAPTER.ONE
                            </p>
                        </div>
                        <div class="ch_01_conts">
                            <div class="ch_01_chart clear">
                                <div class="chart_ani rel" style={{position:'relative',width:'100%',maxWidth:'517px',margin:'0 auto',overflow:'hidden'}}>
                                    <div>
                                        <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/graph/graph_01.png')} style={{width:'100%',maxWidth:'517px'}}/>
                                    </div>
                                    <div class="abs" style={{position:'absolute',top:'0',left:'0',height:'100%'}}>
                                        <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/graph/graph_02.png')} style={{width:'100%', maxWidth: '517px'}}/>
                                    </div>
                                </div>
                                <div class="graph_icons clear" style={{textAlign: 'center',width:'230px',margin:'10% auto',marginBottom:'10%',fontSize:'10px'}}>
                                    <div><span style={{color:'#fcb01a'}}>■</span> 코스피</div>
                                    <div><span style={{color:'#ef4d71'}}>■</span> 가상화폐</div>
                                    <div><span style={{color:'#00ccff'}}>■</span> 코스닥(KOSDAQ)</div>
                                </div>
                                <div class="ch_01_txts">
                                    <p style={{color:'#ffce00',fontSize:'18px',marginBottom:'1rem'}}>
                                        전세계적으로 유통되는 암호화폐
                                    </p>
                                    <p style={{width:'95%',margin:'0 auto',textAlign: 'justify',fontSize:'12px',fontWeight:'normal',marginBottom:'10%',lineHeight:'1.5'}}>
                                        국내 암호화폐시장의 거래규모는 국내 금융시장 또는 다른 국가의암호화폐시장 거래규모에 비해 상당히 크며, 전세계 30% 일일 거래금액을차지하고 있습니다. 암호화폐 전망에 대한 수많은 의견이 쏟아져 나오지만블록체인의 기술발전과 함께 크게 상승 할 것 이라는 분석이 나오고 있으며,미래의 가치적인 수단으로 자리 매김하기 위해 빠른 발전속도와 시장의관심도도 집중되고 있습니다. 안전성과 보안성을 높인 올스타빗에서 시작해보세요.
                                    </p>
                                </div>
                            </div>
                            <div class="width_graphs" style={{marginTop:'95px'}}>
                                <div class="graph_r" style={{width:'100%'}}>
                                    <div class="rel">
                                        <div class="abs percentage01" style={{width:'203px',float:'left'}}></div>
                                        <div class="text_green" style={{float:'left',fontSize:'15px', fontWeight: 'bolder',lineHeight:'24px',float:'right'}}>
                                            52.92%
                                        </div>
                                    </div>
                                    <div style={{fontSize:'15px',fontWeight:'600',textAlign:'right',lineHeight:'1.5',color:'#ffce00'}}>
                                        JPY (일본 엔화)
                                    </div>
                                </div>
                                <div class="graph_l text-right">
                                    <div style={{fontSize:'15px',fontWeight:'600',lineHeight:'1.5',color:'#ffce00',textAlign:'left'}}>
                                        USD (미국 달러)
                                    </div>
                                    <div class="rel">
                                        <div class="abs percentage02" style={{width:'144px',float:'left'}}></div>
                                        <div class="text_green" style={{float:'left',fontSize:'15px',fontWeight: 'bolder',lineHeight:'24px',float:'right'}}>
                                            25.01%
                                        </div>
                                    </div>
                                </div>
                                <div class="width_graphs" style={{marginTop:'0px',color:'#ccc',clear:'both'}}>
                                    <div class="graph_r pt-3" style={{fontSize:'12px',paddingTop:'1rem',fontWeight:'bold',textAlign:'left',lineHeight:'1.5',}}>
                                        <p>
                                            통화별 비트코인 거래량으로 일본의 엔화는 전세계 50%를 넘는 수치로 미국의 달러를 제치고 1위로 거래되고 있습니다.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="width_graphs" style={{marginTop:'50px'}}>
                                <div class="graph_r">
                                    <div class="rel">
                                        <div class="abs percentage03" style={{width:'74px',float:'left'}}></div>
                                        <div class="text_green" style={{fontWeight: 'bolder',float:'right',fontSize:'15px',lineHeight:'25px'}}>
                                            5.79%
                                        </div>
                                    </div>
                                    <div style={{width:'100%',float:'right',fontSize:'15px',fontWeight:'600',lineHeight:'1.5',textAlign:'right',color:'#ffce00'}}>
                                        EUR (유럽 유로)
                                    </div>
                                </div>
                                <div class="graph_l text-right">
                                    <div style={{fontSize:'15px',fontWeight:'600',lineHeight:'1.5',color:'#ffce00',lineHeight:'1.5',textAlign:'left'}}>
                                        KUR (한국 원화)
                                    </div>
                                    <div class="rel">
                                        <div class="abs percentage04" style={{width:'95px',float:'left'}}></div>
                                        <div class="text_green" style={{fontWeight: 'bolder',float:'right',fontSize:'15px',lineHeight:'24px'}}>
                                            12.91%
                                        </div>
                                    </div>
                                </div>
                                <div class="width_graphs clear" style={{marginTop:'0px',color:'#ccc',clear:'both'}}>
                                    <div class="graph_r clear text-right pt-3" style={{fontSize:'12px',paddingTop:'1rem',textAlign:'right',fontWeight:'bold'}}>
                                        <p style={{marginBottom:'1rem'}}>
                                            통화별 비트코인 거래량으로 대한민국은 12.91%로 전세계 3위를 기록하는 통계수치입니다.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="jh" style={{marginTop:'20%',color:'#ffce00',fontSize:'17px',letterSpacing:'15px',marginBottom:'50px',fontWeight:'400'}}>
                        <p style={{wordBreak:'break-all'}}>
                            CHAPTER.TWO
                        </p>
                    </div>

                    <div class="pb-5 rel clear" style={{position:'relative',padding:'0 0 3rem',margin:'0'}}>
                        <div class="col-12 ">
                            <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/graph/world.png')} style={{width:'100%',maxWidth:'290px'}}/>
                        </div>
                        <div>
                            <ul class="stars">
                                <li class="abs"></li>
                                <li class="abs"></li>
                                <li class="abs"></li>
                                <li class="abs"></li>
                                <li class="abs"></li>
                                <li class="abs"></li>
                                <li class="abs"></li>
                                <li class="abs"></li>
                            </ul>
                        </div>

                        <div class="col-12 pt-5" style={{margin:'0 auto',fontSize:'17px',color:'#ffce00',paddingTop:'3rem',paddingLeft:'15px',paddingRight:'15px',fontWeight:'bold',lineHeight:'1.5'}}>세계화되어 가는 암호화폐 거래</div>
                        <div class="col-12 pt-3" style={{paddingTop:'1rem',paddingLeft:'15px',paddingRight:'15px',textAlign:'justify',fontWeight:'normal',fontSize:'12px',lineHeight:'1.5'}}>
                            전자상거래는 전 세계에서 가장 급격하게 성장하고 있는 기술시장 중 하나입니다. 이러한 성장은 우선적으로 인터넷의 광범위한 사용으로 인한 것입니다. 현재도 여러나라 곳곳에서 암호화폐를 통한 거래가 이루어지고 있으며, 다양한 상품과 서비스의 교환을 가능하게하고 단순하면서도 안전한 결제 플랫폼 시스템을 만들어 코인을 보유한 사용자들에게 광범위한 혜택을 누릴 수 있도록 제공할 것입니다.
                        </div>
                    </div>
                    <div class="circles pb-5 rel" style={{position:'relative',padding:'0 0 3rem',margin:'0',display:'flex',flexWrap:'wrap'}}>
                        <div class="abs" style={{position:'absolute'}}>
                            <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/graph/circle_01.png')} style={{width:'100%', maxWidth:'170px'}} />
                        </div>
                        <div class="abs" style={{position:'absolute'}}>
                            <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/graph/circle_02.png')} style={{width:'100%',maxWidth:'110px'}} />
                        </div>
                        <div class="abs" style={{position:'absolute'}}>
                            <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/graph/circle_03.png')} style={{width:'100%',maxWidth:'140px'}} />
                        </div>

                        <div class="col-12 pt-5" style={{margin:'0 auto',fontSize:'17px',color:'#ffce00',marginTop:'77%',width:'100%',paddingTop:'3rem',paddingLeft:'15px',paddingRight:'15px',lineHeight:'1.5',fontWeight:'600'}}>올스타빗만의 코인보관방식</div>
                        <div class="col-12 pt-3" style={{textAlign:'justify',fontWeight:'normal',fontSize:'12px',paddingTop:'1rem',paddingLeft:'15px',paddingRight:'15px',lineHeight:'1.5'}}>
                            모든 신규 예금은 어떤 온라인 시스템에서도 완전한 에어 갭 격리가 가능한 콜드 지갑으로 바로 가게 됩니다. 대부분의 코인은 어떤 온라인 시스템에서도 완전한 에어 갭이 격리된 상태로 차가운 지갑에 저장되어 있으며, 제한된 수의 코인은 드라이브가 잠긴 상태로 보호된 시스템의 반냉각 지갑에 저장됩니다. 운영상의 유동성 유지를 위해 필요한 코인만 핫(온라인)지갑에 저장되게 되며, 모든 지갑은 암호화 되어 있습니다.
                        </div>
                    </div>
                </div>
            </div>

            <div class="app_conts pt-5 pb-5" style={{paddingTop:'3rem',paddingBottom:'3rem'}}>
                <div class="container">
                    <div class="col-12" style={{padding:'3% 0 10% 0'}}>
                        <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/mobile.png')} style={{width:'100%',maxWidth:'250px'}} />
                    </div>
                    <div style={{fontSize:'20px',textAlign: 'center',fontWeight:'500',lineHeight:'1.5'}}>
                        누구나 쓰기 편하고 보기 좋은<br /> 디자인으로 최적화되어 빠르고<br /> 간편한 <span style={{color:'#ffce00',fontWeight: 'bold'}}>모바일 앱서비스 출시예정!</span>
                    </div>
                    <br />
                    <div style={{fontSize:'10px',fontWeight:'500', textAlign:'center'}}>최고의 안전성을 구축하기 위해 최고 수준의 파트너사와 함께합니다.</div>

                    <div class="pt-4" style={{padding:'1.5rem 15px 0 15px',margin:'0'}}>
                        <div id="app_btn1" class="col-12 mb-2 pb-2 justify-content-center align-items-center" style={{height:'auto',border:'1px solid white', borderRadius:'20px',background:'#414143',fontSize:'13px',color:"#ffce00",clear:'both',padding:'0 0 0.5rem',margin:'0 0 0.5rem'}}>
                            <div style={{width:'fit-content',margin:'0 auto',display:'flex'}}>
                                <div class="col-2" style={{float:'left'}}>
                                    <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/apple.png')} style={{marginTop:'10px',paddingLeft:'15px',paddingRight:'15px'}} />
                                </div>
                                <div class="col-5" style={{textAlign: 'left',float:'left',paddingLeft:'15px',paddingRight:'15px',fontWeight:'bold',lineHeight:'1.5'}}>
                                    <span style={{fontSize:'10px',lineHeight:'1.5'}}>available on</span><br /> Apple Store
                                </div>
                            </div>
                        </div>

                        <div id="app_btn2" class="col-12 mb-2 pb-2 justify-content-center align-items-center" style={{height:'auto',border:'1px solid white', borderRadius:'20px', background:'#414143', fontSize:'13px', color:'#ffce00',clear:'both',padding:'0 0 0.5rem',margin:'0 0 0.5rem'}}>
                            <div style={{width:'fit-content',margin:'0 auto',display:'flex'}}>
                                <div class="col-2" style={{float:'left'}}>
                                    <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/and.png')} style={{marginTop:'10px',paddingLeft:'15px',paddingRight:'15px'}} />
                                </div>
                                <div class="col-5" style={{textAlign: 'left',float:'left',paddingLeft:'15px',paddingRight:'15px',fontWeight:'bold',lineHeight:'1.5'}}>
                                    <span style={{fontSize:'10px',lineHeight:'1.5'}}>available on</span><br /> Google Store
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="footer_nav" style={{padding:'10px 0',margin:'0'}}>
                <div class="col-4"><Link to="notice">공지사항</Link></div>
                <div class="col-4" style={{borderRight:'1px solid #e5e5e5', borderLeft:'1px solid #e5e5e5'}}>
                    <Link to="">FAQ</Link>
                </div>
                <div class="col-4"><Link to="/wallet">계정관리</Link></div>
            </div>

            <div class="footer_wraps" style={{paddingTop:'10px'}}>
                <div class="footer_partner" style={{padding:'10px 0',margin:'0'}}>
                    <div class="col-12 mb-4" style={{fontSize:'25px',margin:'0 auto 1.5rem',padding:'10px 15px 0'}}>PARTNERS</div>
                    <div class="col-6" style={{marginBottom:'20px'}}><img src={('http://allstarbit.com/m_exchange/img/partner/samsung.png?=v3')} style={{width:'100px'}} /></div>
                    <div class="col-6" style={{marginBottom:'20px'}}><img src={('http://allstarbit.com/m_exchange/img/partner/bitgo.png?=v3')} style={{width:'100px'}} /></div>
                    <div class="col-6" style={{marginBottom:'20px'}}><img src={('http://allstarbit.com/m_exchange/img/partner/mttlogo.png')} style={{width:'150px'}} /></div>
                    <div class="col-6" style={{marginBottom:'20px'}}><img src={('http://allstarbit.com/m_exchange/img/partner/lucy.png')} style={{width:'140px'}} /></div>
                    <div class="col-6" style={{marginBottom:'20px'}}><img src={('http://allstarbit.com/m_exchange/img/partner/sketch.png')} style={{width:'140px'}} /></div>
                    <div class="col-6" style={{marginBottom:'20px'}}><img src={('http://allstarbit.com/m_exchange/img/partner/kt.png?=v3')} style={{width:'100px'}} /></div>
                </div>
                <div style={{width:'100%',height:'auto',padding:'20px 0 0'}}>
                    <p style={{color:'#999',textAlign:'center'}}>올스타빗은 크롬에 최적화되어 있습니다</p>
                </div>
                <div class="container" style={{width:'calc(100% - 30px)',padding:'10px 15px 0',margin:'0 auto', lineHeight:'20px',textAlign:'left'}}>
                    <div class="row" style={{width:'fit-content',padding:'20px 0 0'}}>
                        <div class="pb-3" style={{width:'fit-content',padding:'10px 0 1rem', marginRight:'10px'}}>
                            <img src={('http://allstarbit.com.main.s3-website.ap-northeast-2.amazonaws.com/img/logo.png')} style={{width:'150px',verticalAlign:'middle'}}/>
                        </div>
                        <div class="col-xl-7 col-xs-12 pb-5" style={{paddingBottom:'3rem',fontSize:'12px',textAlign:'justify'}}>
                            <b style={{fontSize:'14px'}}>올스타빗 암호화폐거래소</b>
                            <br/>(주)올스타 메니지먼트<br/>인천광역시 서구 담지로8번길 7-14, 1층(연희동).<br/> 사업자 등록번호 : 489-81-00823<br/> 통신판매업신고 : 2018-인천서구-0450호<br/>
                            대표 : 최강일,민병진<br/><br/> 문의 : inc@allstarbit.com l 1670-3512<br/> 시간 : 평일 09:00 ~ 18:00 (점심시간 12:30 ~ 13:30)<br/> 고객 상담은 전화, 채팅, 이메일로 진행합니다.(카카오톡 24시간 상담 가능)<br/><br/><br/>
                            <b style={{fontSize:'10px'}}>COPYRIGHT 2018ⓒALLSTARBIT ALL RIGHT RESERVED.</b>
                        </div>
                    </div>
                </div>
            </div>

            </article>
        </section>
    );
  }
}

export default Exchangemain;