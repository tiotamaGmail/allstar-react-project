var path = require('path');
var webpack = require('webpack');
//const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
	devtool: 'source-map',
	entry: {
		"chartIQ": "./src/index.js"
	},
	output: {
		publicPath:'/dist/',
		path: path.resolve(__dirname, './dist'),
		filename: "[name].js"
	},
	module: {
		loaders: [{
			exclude: [/node_modules/, "/chartiq/"],
			loader: 'babel-loader',
			query: {
				presets: ['react', 'es2015', 'stage-2']
			}
		},
		{
			test: /\.css$/,
			loader: 'style-loader'
		},
		{
			test: /\.css$/,
			loader: 'css-loader',
			options: {
				importLoaders: 1,
				modules: true,
				localIdentName: "[name]__[local]___[hash:base64:5]"  
			},
		},
		{
			test: /\.(jpe?g|png|gif|svg|woff2?|ttf|eot)$/i,
			use: [
					{ loader:require.resolve("file-loader") + "?name=../[path][name].[ext]" }
			]
		}]
	},
	plugins: [
		new webpack.DefinePlugin({
			/* "process.env": {
				NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'production')
			} */
		})
	],
	devServer: {
		port: 3000,
		compress: true,
		inline: true,
		stats: 'minimal'
	},
	resolve: {
		extensions: ['.js', '.jsx']
	},
	
};
