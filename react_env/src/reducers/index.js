import { combineReducers } from 'redux'
import { sessionReducer } from 'redux-react-session';
/*import chart from './chartReducer'
import draw from './drawingReducer'
import study from './studyReducer'
import theme from './themeReducer'*/
import authentication from './authentication';

const reducer = combineReducers({
    authentication,
 /*   chart,
    draw,
    study,
    theme,*/
		session: sessionReducer
});

export default reducer
