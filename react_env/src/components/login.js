import React from 'react';
import {FormGroup} from "react-bootstrap";
import axios from 'axios';
import {Link} from "react-router-dom";
import { connect } from 'react-redux';
import { sessionService } from 'redux-react-session';

import { loginRequest, pj_code } from '../actions/authentication';

const mapStateToProps = (state) => {
  return {
      status: state.authentication.login.status
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
      loginRequest: (id, pw) => {
          return dispatch(loginRequest(id,pw));
      }
  };
};

class Login extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    };
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePwd = this.handleChangePwd.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    
  }
  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }
  handleChangeEmail(event) {
    this.setState({email : event.target.value});
  }
  handleChangePwd(event) {
    this.setState({password : event.target.value});
  }
  handleSubmit = event => {
    event.preventDefault();
    const id = this.state.email;
    const pw = this.state.password;
    
    return this.props.loginRequest(id, pw).then(
      () => {
        if(this.props.status === "SUCCESS") {

        	const sessionInfo = {
        		email: id,
        		password: pj_code(pw)
					};
	
					sessionService.saveSession()
						.then(() => sessionService.saveUser(sessionInfo))
						.then(() => {
							this.props.history.push("/");
						})
						.catch((err) => {
							console.log('err: ', err);
						});
          // create session data
          // let loginData = {
          //     isLoggedIn: true,
          //     username: id,
          //     password: pw
          // };
          // document.cookie = 'key=' + btoa(JSON.stringify(loginData));
          // this.props.history.push("/");
          //Materialize.toast('Welcome, ' + id + '!', 2000);
          //browserHistory.push('/');
          return true;
        } else {
            //let $toastContent = $('<span style="color: #FFB4BA">Incorrect username or password</span>');
            //Materialize.toast($toastContent, 2000);
            alert("false")
            return false;
        }
      }
    );
  }
  render(){
    return(
      <section className="loginCenter" id="loginCenter">
        <article id="loginArticle1">
          <div className="loginDiv">
            <div className="Logheading">로그인</div>
            <div className="logfrm">
              <form onSubmit={this.handleSubmit}>
                <FormGroup controlId="email"><input type="email" placeholder="이메일 입력" onChange={this.handleChangeEmail.bind(this)}/></FormGroup>
                <FormGroup controlId="password"><input type="password" placeholder="비밀번호 입력" onChange={this.handleChangePwd.bind(this)}/></FormGroup>
                <button type="submit" disabled={!this.validateForm()}>로그인</button>
              </form>
            </div>
            <div className="logtxt">
              <p>아직 올스타빗 계정이 없으신가요?<Link to="/register">회원가입</Link></p>
              <p>로그인에 문제가 있으신가요?<Link to="/passwordset">패스워드 재설정</Link></p>
            </div>
          </div>
        </article>
      </section>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
