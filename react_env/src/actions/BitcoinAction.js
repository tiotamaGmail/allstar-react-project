import createTypes from 'redux-create-action-types'

/*
 * action types
 */
const Types = createTypes(
    'PRE_BITCOIN',
    'NEXT_BITCOIN',
    'FAVORITE_BITCOIN',
);

export default Types;