import React from 'react';
import jsSHA from 'jssha';

class Authotp extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      secret:'JBSWY3DPEHPK3PXP',
      key:'',
      hmac:'',
      otp: 0,
      inputOTP:0
    }

    this.timer = 0;
    this.tick = this.tick.bind(this);
    this.updateOtp = this.updateOtp.bind(this);
    this.dec2hex = this.dec2hex.bind(this);
    this.hex2dec = this.hex2dec.bind(this);
    this.base32tohex = this.base32tohex.bind(this);
    this.leftpad = this.leftpad.bind(this);
    this.handleOTP = this.handleOTP.bind(this);
    this.confirmOTP = this.confirmOTP.bind(this);
  }

  componentDidMount(){
    this.timer = setInterval(this.tick, 1000);
  }

  componentWillUnmount(){
    clearInterval(this.timer);
  }

  tick(){
    this.updateOtp();
  }

  dec2hex(s) { return (s < 15.5 ? '0' : '') + Math.round(s).toString(16); }
  hex2dec(s) { return parseInt(s, 16); }
  base32tohex(base32) {
    var base32chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
    var bits = "";
    var hex = "";

    for (var i = 0; i < base32.length; i++) {
      var val = base32chars.indexOf(base32.charAt(i).toUpperCase());
      bits += this.leftpad(val.toString(2), 5, '0');
  }

    for (var i = 0; i+4 <= bits.length; i+=4) {
      var chunk = bits.substr(i, 4);
      hex = hex + parseInt(chunk, 2).toString(16) ;
    }
    return hex;
  }

  leftpad(str, len, pad) {
    if (len + 1 >= str.length) {
        str = Array(len + 1 - str.length).join(pad) + str;
    }
    return str;
  }

  updateOtp() {
    var key = this.base32tohex(this.state.secret);
    var epoch = Math.round(new Date().getTime() / 1000.0);
    var time = this.leftpad(this.dec2hex(Math.floor(epoch / 30)), 16, '0');

    // updated for jsSHA v2.0.0 - http://caligatio.github.io/jsSHA/
    var shaObj = new jsSHA("SHA-1", "HEX");
    shaObj.setHMACKey(key, "HEX");
    shaObj.update(time);
    var hmac = shaObj.getHMAC("HEX");

    if (hmac !== 'KEY MUST BE IN BYTE INCREMENTS') {
      var offset = this.hex2dec(hmac.substring(hmac.length - 1));
    }

    var otp = (this.hex2dec(hmac.substr(offset * 2, 8)) & this.hex2dec('7fffffff')) + '';
    otp = (otp).substr(otp.length - 6, 6);

    this.setState({
      key:key,
      hmac:hmac,
      otp:otp
    });
  }
  
  handleOTP(event){
    console.log(event.target.value);
    this.setState({inputOTP : event.target.value});
  }
  
  confirmOTP(){
    if(this.state.otp === this.state.inputOTP)
      alert("SUCCESS");
    else
      alert("FAIL");
  }

  render(){
    const drawQR = () => {
      return <img src={"https://chart.googleapis.com/chart?chs=200x200&cht=qr&chl=200x200&chld=M|0&cht=qr&chl=otpauth://totp/Allstarbit%3Fsecret%3D"+this.state.secret} />
    }
    return(
      <section id="authOtpCenter">
        <article id="authOtpArticle1">
          <div id="authOtpDiv">
            <div className="authOtpHeader">
                <p>보안 - 2FA 활성화</p>
                <p>
                  당사는 고객님의 정보를 귀중히 여기며,<br />
                  안전하게 보관될 수 있도록 최선을 다합니다.
                </p>
            </div>
            <div className="authOtpBody1">
              <ul className="authOtpDownload">
                <li><p>STEP 1. 2FA 앱을 다운로드해 주십시오.</p></li>
                <li><p>Google Play Store 및 Apple Store에서 아래의 2FA 인증 앱을 휴대기기에 다운로드해 주십시오.</p></li>
                <li>
                  <a>
                    <p><img src={require("../img/google_play.png")} /></p>
                    <p><a href="https://market.android.com/details?id=com.google.android.apps.authenticator&hl=en">Google OTP</a></p>
                  </a>
                </li>
                <li>
                  <a>
                    <p><img src={require("../img/app_store.png")} /></p>
                    <p><a href="http://itunes.apple.com/au/app/google-authenticator/id388497605?mt=8">Google authenticator</a></p>
                  </a>
                </li>
              </ul>
              <ul className="authOtpQrcode">
                <li><p>STEP 2. 바코드 스캔 또는 16자리 코드번호 입력</p></li>
                <li>
                  <p>
                    위에서 설치한 2FA 인증 앱을 실행한 후, '바코드 스캔'을 실시하여 아래에 표시된 QR 코드를 스마트폰으로 스캔해 주십시오. 또는 '제공된 키 입력'을 선택하시어 16자리 코드번호를 입력하셔도 됩니다.
                    <br />
                    <a>2FA는 어떻게 설정하면 되나요?</a>
                  </p>
                </li>
                <li>
                  <p>QR 코드</p>
                  <p>{drawQR()}</p>
                </li>
                <li>
                  <p>16자리 코드 번호</p>
                  <p><input type="text" value={this.state.secret} readonly /></p>
                </li>
              </ul>
              <ul className="authOtpQrcodeInsert">
                <li><input type="text" placeholder="2FA 코드 번호 입력" onChange={this.handleOTP.bind(this)}/></li>
                <li><a onClick={this.confirmOTP.bind(this)}>본인인증 완료</a></li>
              </ul>
            </div>
          </div>
        </article>
      </section>
    );
  }
}

export default Authotp;