$(document).ready(function(){
    //trade_detail sub tab
    $(".detailRight0Menu li").each(function(idx){
       $(this).click(function(){
           switch(idx){
               case 0:
                    $(this).css({
                        "box-shadow":"inset 0px -4px 0px #ffa500",
                        "color":"#ffa500"
                    }); 
                    $(".detailRight0Menu li").not($(this)).css({"color":"#fff"});
                break;
                   
                case 1:
                    $(this).css({
                        "box-shadow":"inset 0px -4px 0px #02d72f",
                        "color":"#02d72f"
                    }); 
                    $(".detailRight0Menu li").not($(this)).css({"color":"#fff"});
                break;
                   
               case 2:
                    $(this).css({
                       "box-shadow":"inset 0px -4px 0px #fff",
                       "color":"#fff"
                    });
                    $(".detailRight0Menu li").not($(this)).css({"color":"#fff"});
                break;
           }
           $(".detailRight"+idx+"Center").css({"display":"block"});
           $(".detailRight0Menu li").not($(this)).css({"box-shadow":"0px 0px 0px #333"});
           $(".tradeDetailRight0 > div").not(".detailRight"+idx+"Center").css({"display":"none"});
       }); 
    });
    
   //tradebody list sub tab
    $(".tradeDetailMenu li").each(function(idx){
       $(this).click(function(){
           $(".tradeDetailBody"+idx).css({"display":"block"});
           $(this).css({"color":"#ffa500"});
           $(".tradeDetailMenu li").not($(this)).css({"color":"#fff"});
           for(var i=0; i<5; i++){
                $("#article1 .tradeDetailBody"+i).not(".tradeDetailBody"+idx).css({
              "display":"none"
           });
           }
       }); 
    });
    
    //mobileQuoteHead color
    $(".mobileQuoteHead li").each(function(idx){
        $(this).click(function(){
            $(".mobileQuoteHead li").eq(idx).css({
                "color":"#ffa500",
                "border":"1px solid #ffa500"
            });
            $(".mobileQuoteHead li").not($(this)).css({
                "color":"#fff",
                "border":"1px solid #fff"
           });

        });
    });
    
    //notice head color
    $(".noticeHead li").each(function(idx){
       $(this).click(function(){
            $(this).css({"color":"#ffa500"});
            $(".noticeHead li").not($(this)).css({"color":"#fff"});
       }); 
    });
    
    $(".investmentHead li").each(function(idx){
       $(this).click(function(){
            $(this).css({
                "color":"#ffa500",
                "box-shadow":"0px 3px 0px #ffa500"
            });
           $(".investment"+idx+"Content").css({"display":"block"});
            for(var i=0; i<3; i++){
                $("#article1 .investment"+i+"Content").not(".investment"+idx+"Content").css({"display":"none"});
            }  
           
            $(".investmentHead li").not($(this)).css({
                "color":"#fff",
                "box-shadow":"0px 0px 0px #ffa500"
            });

       }); 
    });
    
    
    //특정 option selected 
    $(".detailRight0pricePercent").click(function(){
        if($(this).height()==35){
           $(this).css({
              "height":"175px",
              "overflow-y":"scroll"
           }); 
            $(this).animate({scrollTop:"665px"});
        }else{
           $(this).css({
              "height":"35px",
              "overflow":"hidden"
           });
           $(this).animate({scrollTop:"0px"},0);
        }
   });
    //매수 가능
    $(".detailRight0Amount p").click(function(){
       if($(".detailRight0Amount ul").css("display")=="none"){
           $(".detailRight0Amount ul").css({"display":"block"});
       }else{
           $(".detailRight0Amount ul").css({"display":"none"});
       }
    });
    //매도 가능
    $(".detailRight1Amount p").click(function(){
       if($(".detailRight1Amount ul").css("display")=="none"){
           $(".detailRight1Amount ul").css({"display":"block"});
       }else{
           $(".detailRight1Amount ul").css({"display":"none"});
       }
    });
});//document ready close