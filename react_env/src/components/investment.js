import React from 'react';
import Tabs, { Tab } from 'react-awesome-tabs';
import { connect } from 'react-redux';
import axios from 'axios';


const mapStateToProps = (state) => {
  if(state.session.authenticated)
  {
    return {
      user:state.session.user.email,
      password:state.session.user.password,
      isLogin:state.session.authenticated
    };
  }
};

class Investment extends React.Component{
  constructor(props){
    super(props);
    var userData = {
      isLogin : this.props.isLogin,
      user : this.props.user,
      password : this.props.password
    };

    this.state = {
      userData:userData,
      koreaWalletInfo: {},
      coinWalletInfo: [],
      transactionList: [],
      conclusionList:[],
      activeTab: 0,
      totalBalance : 0
    };

    this.timer = 0;
    this.tick = this.tick.bind(this);
    this.walletUploadData = this.walletUploadData.bind(this);
    this.loadTransactionData = this.loadTransactionData.bind(this);
    this.loadConclusionData = this.loadConclusionData.bind(this);
    
    this.walletUploadData = this.walletUploadData.bind(this);
    if(this.state.userData.isLogin){
      if(this.state.activeTab === 0)
        this.walletUploadData(this.props.source, this.props.target);
      else if(this.state.activeTab === 1)
        this.loadTransactionData();
      else if(this.state.activeTab === 2)
        this.loadConclusionData();
    }
  }

  componentDidMount(){
    this.timer = setInterval(this.tick, 1000);
  }

  componentWillUnmount(){
    clearInterval(this.timer);
  }

  tick(){
    if(this.state.userData.isLogin){
      if(this.state.activeTab === 0)
        this.walletUploadData(this.props.source, this.props.target);
      else if(this.state.activeTab === 1)
        this.loadTransactionData();
      else if(this.state.activeTab === 2)
        this.loadConclusionData();
    }
  }

  walletUploadData(source, target){
    let data = {
      'REQ': 'user_wallet_read',
      'email':this.props.user,
      'password':this.props.password
    };
    var totalBalance = 0;
    axios.post('http://api.allstarbit.com/req.php', data)
    .then(response => {
      console.log(response.data);
      if(response.data){
        var koreaWalletInfo = response.data.find(function(element) {
          if(element.issue === "KRW"){
            return element;
          } 
        });
        var coinWalletInfo = response.data.map(function(element) {
          if(element.balance > 0){
            totalBalance += totalBalance;
            return element;
          } 
        });

        this.setState({
          totalBalance : totalBalance,
          koreaWalletInfo: koreaWalletInfo,
          coinWalletInfo : coinWalletInfo
        });
      }
    })
  }

  loadConclusionData(){
    var rightNow = new Date();
    var finishDate = rightNow.toISOString().slice(0,10);
    rightNow.setDate(rightNow.getDate() - 1);
    var startDate = rightNow.toISOString().slice(0,10);

    var data = {
      'REQ': 'user_wallet_transaction_read_waiting',
      'email':this.props.user,
      'password':this.props.password,
      'startDate': startDate,
      'finishDate': finishDate,
      'pageNumber': 1,
      'pageCount':10,
      'source':"KRW",//this.state.source,
      'target': "BTC"//this.state.target
    };
   
    // API REQUEST
    return axios.post('http://api.allstarbit.com/req.php', data).then((response)=>{
      console.log(response);
      var list = [];
      if(response.data)
      {
        list = response.data.map(data=>{
          return data;
        });
      }
      this.setState({conclusionList:list,transactionList:[]});
    }).catch((error)=>{
        // FAILED
        console.log(error);
    });
  }

  loadTransactionData(){
    var rightNow = new Date();
    var finishDate = rightNow.toISOString().slice(0,10);
    rightNow.setDate(rightNow.getDate() - 1);
    var startDate = rightNow.toISOString().slice(0,10);

    var data = {
      'REQ': 'user_wallet_transaction_read',
      'email':this.props.user,
      'password':this.props.password,
      'startDate': startDate,
      'finishDate': finishDate,
      'pageNumber': 1,
      'pageCount':10
    };
   
    // API REQUEST
    return axios.post('http://api.allstarbit.com/req.php', data).then((response)=>{
      console.log(response);
      if(response.data && response.data.complete)
        this.setState({transactionList:response.data.complete,conclusionList:[]});
    }).catch((error)=>{
        // FAILED
        console.log(error);
    });
  }

  handleTabSwitch(active) {
		this.setState({ activeTab: active });
	}

  render(){

    return(
      <section id="investmentCenter">
        <article id="article1">
          <div className="investmentTitle">투자내역</div>

          <Tabs	active={ this.state.activeTab }	onTabSwitch={ this.handleTabSwitch.bind(this) }>
            <Tab title="보유코인">
              <div className="investment0Content clear">
                <table>
                  <tbody>
                    <tr><td colSpan="2">내 보유자산</td></tr>
                    <tr>
                      <td>
                        <p>보유KRW</p>
                        <p>0</p>
                      </td>
                      <td>
                        <p>총보유자산</p>
                        <p>56,120</p>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <span>총매수</span>
                        <span>123,157,987</span>
                      </td>
                      <td>
                        <span>평가손익</span>
                        <span>-78,338</span>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <span>총평가</span>
                        <span>56,120</span>
                      </td>
                      <td>
                        <span>수익률</span>
                        <span>-58.20%</span>
                      </td>
                    </tr>
                  </tbody>
                </table>
                <table> 
                  <tbody>
                    <tr>
                      <td>
                        <p>스테이터스네트워크토큰</p>
                        <p>(SNT)</p>
                      </td>
                      <td>
                        <p>
                          <span>평가손익</span>
                          <span>-78,336</span>
                        </p>
                        <p>
                          <span>수익률</span>
                          <span>-58.20%</span>
                        </p>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p>
                          <span>567.15781578</span>
                          <span>SNT</span>
                        </p>
                        <p>보유수량</p>
                      </td>
                      <td>
                        <p>
                          <span>239</span>
                          <span>KRW</span>
                        </p>
                        <p>매수평균가</p>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <p>
                          <span>127,157</span>
                          <span>KRW</span>
                        </p>
                        <p>평가금액</p>
                      </td>
                      <td>
                        <p>
                          <span>127,997</span>
                          <span>KRW</span>
                        </p>
                        <p>매수금액</p>
                      </td>
                    </tr>
                  </tbody>
                </table>    
              </div>
            </Tab>
            <Tab title="거래내역">
              <div className="investment1Content clear">
                <div>
                  <p>거래내역 조회</p>
                  <select>
                    <option>거래전체</option>
                    <option>매수</option>
                    <option>매도</option>
                    <option>입금</option>
                    <option>출금</option>
                  </select>
                </div>
                <table className="investment1Content1T">
                  <thead>
                    <tr>
                      <th>주문시간</th>
                      <th>거래종류</th>
                      <th>거래단가</th>
                      <th>수수료</th>
                    </tr>
                    <tr>
                      <th>코인</th>
                      <th>거래수량</th>
                      <th>거래금액</th>
                      <th>정산금액<br /><span>(수수료반영)</span></th>
                    </tr>
                  </thead>
                  {
                    this.state.transactionList.map(data=>{
                      return(
                        <tbody> 
                          <tr>
                            <td>{data.procDate}</td>
                            {(data.askOrBid===1)?
                            <td><span className="investment1ContentRed">매수</span></td>:
                            <td><span className="investment1ContentBlue">매도</span></td>
                            }
                            <td>{data.requestValue}</td>
                            <td>0</td>
                          </tr>
                          <tr>
                            <td>{data.coins}</td>
                            <td>{data.volume}</td>
                            <td>{(data.requestValue * data.volume).toFixed(0)}</td>
                            <td>{(data.requestValue * data.volume).toFixed(0)}</td>
                          </tr>
                        </tbody>
                      )
                    })
                  }
                </table>
              </div>
            </Tab>
            <Tab title="미체결">
              <div className="investment2Content clear">
                <div>
                  <p>미체결내역</p>
                  <p>
                    <a>전체주문 취소</a>
                    <a>선택주문 취소</a>
                  </p>
                </div>
                <table className="investment2Content1T">
                  <thead>
                    <tr>
                      <th rowSpan="2"></th>
                      <th>마켓명</th>
                      <th>주문수량</th>
                      <th>체결수량</th>
                      <th rowSpan="2">주문시간</th>
                    </tr>
                    <tr>
                      <th>거래종류</th>
                      <th>주문가격</th>
                      <th>미체결수량</th>
                    </tr>
                  </thead>
                  {
                    this.state.conclusionList.map(data=>{
                      return(
                        <tbody> 
                          <tr>
                            <td rowSpan="2"><input type="radio" /></td>
                            <td>코인</td>
                            <td>14.23925255</td>
                            <td>2.33930400</td>
                            <td rowSpan="2">17.12.03<br />20:42</td>
                          </tr>
                          <tr>
                            <td><span className="investment2ContentRed">매수</span></td>
                            <td>0.00330001</td>
                            <td>11.89994855</td>
                          </tr>
                        </tbody>
                      )
                    })
                  }
                </table>
              </div>
            </Tab>
          </Tabs>

        </article>
      </section>
    );
  }
}
export default connect(mapStateToProps, null)(Investment);