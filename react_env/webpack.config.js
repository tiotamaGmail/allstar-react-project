var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	devtool: 'source-map',
	entry: {
		"chartIQ": "./src/index.js",
		"css":"./chartiq/css/chartiq.css"
	},
	output: {
		publicPath:'/dist/',
		path: path.resolve(__dirname, './dist'),
		filename: "[name].js"
	},
	module: {
		loaders: [{
			exclude: [/node_modules/, "/chartiq/"],
			loader: 'babel-loader',
			query: {
				presets: ['react', 'es2015', 'stage-2']
			}
		},
		{
			test: /\.(html)$/,
			use: {
			  loader: 'html-loader',
			  options: {
				attrs: [':data-src']
			  }
			}
		},
		{
			test: /\.(pdf|jpg|png|gif|svg|ico)$/,
			use: [
				{
					loader: 'url-loader'
				},
			]
		},
		{
			test: /\.css$/,
			use: [ 'style-loader', 'css-loader' ]
		}]
		
	},
	plugins: [new HtmlWebpackPlugin({
        template: './index.html',
        filename: 'index.html',
        inject: 'body'
	})],
	// [
	// 	new webpack.DefinePlugin({
	// 		/* "process.env": {
	// 			NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'production')
	// 		} */
	// 	})
	// ],
	devServer: {
		port: 3000,
		compress: true,
		inline: true,
		stats: 'minimal'
	},
	resolve: {
		extensions: ['.js', '.jsx']
	}	
};
