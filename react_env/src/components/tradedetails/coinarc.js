import React from 'react';
import axios from 'axios';

class Coinarc extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      target: this.props.target,
      source: this.props.source,
      prevClosingPrice:1,
      highPrice:0,
      lowPrice:0,
      curTradePrice:0,
      highVolume : 1,
      askInfoList: [],
      bidInfoList: [],
      dailyInfoList : [],
      totalAskSize : 0,
      totalBidSize : 0,
      accTradeVolume24h: 0,
      accTradePrice24h:0,
      highest52WeekPrice:0,
      lowest52WeekPrice:0
    }
  
    this.timer = 0;
    this.tick = this.tick.bind(this);
    this.uploadData = this.uploadData.bind(this);
    this.overviewUploadData = this.overviewUploadData.bind(this);
    this.overviewUploadData(this.props.source, this.props.target);
    this.center();    
  }

  componentWillReceiveProps(nextProps) {
    let nextTradePrice = Number(nextProps.curTradePrice.replace(/[^0-9\.]/g,''));
    if(this.state.curTradePrice !== nextTradePrice)
    {
      this.setState({curTradePrice:nextTradePrice});
    }
  }

  componentDidMount(){
    this.timer = setInterval(this.tick, 1000);
  }

  componentWillUnmount(){
    clearInterval(this.timer);
  }

  tick(){
    this.uploadData(this.state.source, this.state.target);
    
    if(!this.state.overviewData){
      this.overviewUploadData(this.state.source, this.state.target);
    }
  }

  overviewUploadData(source, target){
    axios.get('http://view.allstarbit.com/view2/overview.php?source='+source)
    .then(response => {
      if(response.data) {
        this.uploadData(source, target);
        var overview = response.data.find(function(element) {
          if(element.target === target && element.source === source){
            return element;
          } 
        });
        this.setState({
          prevClosingPrice : overview.data.prevClosingPrice,
          highPrice : overview.data.highPrice,
          lowPrice : overview.data.lowPrice,
          accTradeVolume24h : overview.data.accTradeVolume24h,
          accTradePrice24h : overview.data.accTradePrice24h
        });
      }
    });
  }

  uploadData(source, target){
    axios.get('http://view.allstarbit.com/upbit_streams/calls.php?source='+source+'&target='+target)
    .then(response => {
      var highVolume = 0;
      this.setState({
        highest52WeekPrice:response.data.overview.highest52WeekPrice,
        lowest52WeekPrice:response.data.overview.lowest52WeekPrice
      });
      var askInfoList = response.data.calls.orderbookUnits.map(callInfo=>{
        if(callInfo.askSize > highVolume)
          highVolume = callInfo.askSize;
        if(callInfo.bidSize  > highVolume)
          highVolume = callInfo.bidSize;

        if(this.state.source === 'KRW')
        {
          return {
            askPrice: callInfo.askPrice,
            askSize: callInfo.askSize,
          };
        }else {
          return {
            askPrice: callInfo.askPrice,
            askSize: callInfo.askSize,
            krwPrice: callInfo.askKrwPrice
          };
        }
      });

      var bidInfoList = response.data.calls.orderbookUnits.map(callInfo=>{
        if(this.state.source === 'KRW')
        {
          return {
            bidPrice: callInfo.bidPrice,
            bidSize: callInfo.bidSize,
          };
        }else {
          return {
            bidPrice: callInfo.bidPrice,
            bidSize: callInfo.bidSize,
            krwPrice: callInfo.bidKrwPrice
          };
        }
      });

      askInfoList.reverse();
      this.setState({
        highVolume: highVolume,
        askInfoList: askInfoList,
        bidInfoList: bidInfoList,
        totalAskSize : response.data.totalAskSize,
        totalBidSize : response.data.totalBidSize
      });
    });
    axios.get('http://view.allstarbit.com/view2/transaction_list.php?source='+source+'&target='+target+'&limit=20')
    .then(response => {
      if(response.data)
      {
        var dailyInfoList = response.data.map(dailyInfo=>{
          return dailyInfo;
        });
        this.setState({dailyInfoList:dailyInfoList});
      }
      else {
        this.setState({dailyInfoList:[]});
      }
    });
  }

  center(){
    for (let i = 1; i <= 95; i++) {
      setTimeout(() => (this.fixedTableBodyList.scrollTop = i), i * 2);
    }
  }

  render(){
    let {highPrice, lowPrice, prevClosingPrice, highVolume, accTradeVolume24h, accTradePrice24h, curTradePrice,highest52WeekPrice,lowest52WeekPrice} = this.state;
    const tradeStyle = {border:"1px solid #fff"};
    let highRate = Number(((highPrice * 100) / prevClosingPrice) - 100).toFixed(2);
    let lowRate = Number(((lowPrice * 100) / prevClosingPrice) - 100).toFixed(2);

    const quotesTable = () => {

    }
    return(
      <div className="tradeDetailBody1" ref={fixedTableBodyList => this.fixedTableBodyList = fixedTableBodyList}>
        <table className="tradeDetailTable">
          <tbody>
           {this.state.askInfoList.map((data, index)=>{
              let tradePriceNum = Number(data.askPrice.replace(/[^0-9\.]/g,''))
              let priceRate = Number(((tradePriceNum * 100) / prevClosingPrice) - 100).toFixed(2); 
              let volumeRate = Number(((data.askSize * 100) / highVolume));
              if(index === 0)
              {
                return (
                  <tr>
                    <td><p style={{width:volumeRate+'%'}}>&nbsp;<span>{data.askSize}</span></p></td>
                    <td style={(tradePriceNum === curTradePrice)?tradeStyle:null}>{data.askPrice}  <span>{(this.state.source === 'KRW')?priceRate+'%':data.krwPrice}</span></td>
                    <td rowSpan="10">
                      <div className="quoteAmount clear">
                        <p><span>거래량</span><span>{accTradeVolume24h}{this.state.target}</span></p>
                        <p><span>거래금</span><span>{accTradePrice24h}</span></p>
                        <p>(최근24시간)</p>
                      </div>
                      <div className="quoteWeekData clear">
                        <p className="clear"><span>52주최고</span><span>{highest52WeekPrice}</span></p>
                        <p className="clear"><span>52주최저</span><span>{lowest52WeekPrice}</span></p>
                      </div>
                      <div className="quoteDailyData clear">
                        <p><span>전일종가</span><span>{prevClosingPrice}</span></p>
                        <p><span>당일고가</span><span>{highPrice}</span></p>
                        <p align="right">{highRate}%</p>
                        <p><span>당일저가</span><span>{lowPrice}</span></p>
                        <p align="right">{lowRate}%</p>
                      </div>
                    </td>
                  </tr>
                )
              }
              else{
                return (
                  <tr>
                    <td><p style={{width:volumeRate+'%'}}>&nbsp;<span>{data.askSize}</span></p></td>
                    <td style={(tradePriceNum === curTradePrice)?tradeStyle:null}>{data.askPrice}  <span>{(this.state.source === 'KRW')?priceRate+'%':data.krwPrice}</span></td>
                  </tr>
                )
              }
              }
            )}
            {this.state.bidInfoList.map((data, index)=>{
              let tradePriceNum = Number(data.bidPrice.replace(/[^0-9\.]/g,''))
              let priceRate = Number(((tradePriceNum * 100) / prevClosingPrice) - 100).toFixed(2); 
              let volumeRate = Number(((data.bidSize * 100) / highVolume));
         
              if(index === 0){
                return (
                  <tr>
                    <td rowSpan="10">
                      <p className="quoteAmount2"><span>체결가</span><span>체결량</span></p>
                      {
                        this.state.dailyInfoList.map(trade => {
                          return <p className="quoteAmountResult"><span>{trade.tradePrice}</span><span  style={(trade.askBid==="ASK")?{color:"#d24f45"}:{color:"#297eff"}}>{trade.tradeVolume}</span></p>
                        })
                      }
                    </td>
                    <td style={(tradePriceNum === curTradePrice)?tradeStyle:null}>{data.bidPrice} <span>{(this.state.source === 'KRW')?priceRate+'%':data.krwPrice}</span></td>
                    <td><p style={{width:volumeRate+'%'}}>&nbsp;{data.bidSize}</p></td>
                  </tr>
                )
              }
              else {
                return (
                  <tr>
                    <td style={(tradePriceNum === curTradePrice)?tradeStyle:null}>{data.bidPrice} <span>{(this.state.source === 'KRW')?priceRate+'%':data.krwPrice}</span></td>
                    <td><p style={{width:volumeRate+'%'}}>&nbsp;{data.bidSize}</p></td>
                  </tr>
                )
              }
              }
            )}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Coinarc;