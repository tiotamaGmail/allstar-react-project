import React from 'react';
import axios from 'axios';

class CoinInfo extends React.Component{
  
  constructor(props){
    super(props)
    this.state = {
      coinListDetails : {},
      
    }
  }
  
  componentDidMount(){
    if(this.props.target)
    {
      axios.get('http://api.allstarbit.com/description.php?issue='+this.props.target)
      .then(response => {
        var coinDetails = response.data.data;
       console.log(coinDetails);
        this.setState({coinListDetails:coinDetails});
      })
    }
   
  }
  
  render(){
   
   return (Object.keys(this.state.coinListDetails).length)?
    (
      <div className="tradeDetailBody4">
        <div className="mobileCoinDataTitle">
          <ul className="clear">
            <li><img src={require("../../img/"+this.props.target+".png")} alt="BTC" /></li>
            <li>
              <p>{this.state.coinListDetails.korean_name}({this.state.coinListDetails.english_name})</p>
              <p>심볼 {this.state.coinListDetails.symbol}</p>
            </li>
          </ul>
        </div>
        <table className="mobileCoinDataBody0">
          <tbody>
            <tr><th>최초발행</th><td>{this.state.coinListDetails.header_key_values.initial_release_at.value}</td></tr>
            <tr><th>상장거래소</th><td>{this.state.coinListDetails.header_key_values.number_of_exchanges.value}</td></tr>
            <tr><th>블록 생성주기</th><td>{this.state.coinListDetails.header_key_values.block_creation_period.value}</td></tr>
            <tr><th>채굴 보상량</th><td>{this.state.coinListDetails.header_key_values.mining_fee.value}</td></tr>
            <tr><th>총 발행한도</th><td>{this.state.coinListDetails.header_key_values.total_mining_limit.value}</td></tr>
            <tr><th>합의 프로토콜</th><td>{this.state.coinListDetails.header_key_values.protocol.value}</td></tr>
            <tr><th>백서</th><td><a href="{this.state.coinListDetails.header_key_values.white_paper.link}">{this.state.coinListDetails.header_key_values.white_paper.value}</a></td></tr> 
          </tbody>
        </table>
        <div className="mobileCoinDataBody1">
          <div>코인정보</div>
          {this.state.coinListDetails.main_components.list.map(data=>
          <div>
          {data.detail.content}<br/>
          </div>
          )
        }
        </div>
      </div>
    ):(null);
  }
}

export default CoinInfo;