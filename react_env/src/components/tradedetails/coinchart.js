/*import React from 'react';
import axios from 'axios';
import ChartContainer from '../../containers/chartContainer'

class Coinchart extends React.Component{

	constructor(props) {
		super(props);
		this.state = {
			source : this.props.source,
			target : this.props.target,
			tx: 1,
			startDate: 201804301141,
		};
		this.getCoinDetails = this.getCoinDetails.bind(this);
		this.yyyymmdd = this.yyyymmdd.bind(this);
	}

	yyyymmdd() {
    var now = new Date();
    var y = now.getFullYear();
    var m = now.getMonth() + 1;
    var d = now.getDate();
    var mm = m < 10 ? '0' + m : m;
    var dd = d < 10 ? '0' + d : d;
    return '' + y + mm + dd+now.getHours()+now.getMinutes();
	}

	getCoinDetails() {
		var dateStr = this.yyyymmdd();

		axios.get('http://view.allstarbit.com/upbit_streams/chart_init.php?issue=' + this.props.source+'-'+this.props.target + '&tx=' + this.state.tx + '&startDate=' + this.state.startDate + '&endDate=' + dateStr).then(response => {
			console.log('>>>>>>>>>>>>>>>', response.data);
		})
	}
  render(){
    return(
      <div className="tradeDetailBody2">
        <ul className="tradeDetail2Chart clear">
          <li><span>KST</span><img src={require("../../img/btn_arrow.png")} alt="arrow" /></li>
          <li><span>10분</span><img src={require("../../img/btn_arrow.png")} alt="arrow" /></li>
          <li><span>테마</span><img src={require("../../img/btn_arrow.png")} alt="arrow" /></li>
          <li><span>설정 초기화</span></li>
        </ul>
        <div className="mobileChartArea">
          <ChartContainer onLoad={this.getCoinDetails()}/>
        </div>
      </div>
    );
  }
}*/
import React from 'react';


class Coinchart extends React.Component{
  render(){
    return(
      <div className="tradeDetailBody2">
        <ul className="tradeDetail2Chart clear">
          <li><span>KST</span><img src={require("../../img/btn_arrow.png")} alt="arrow" /></li>
          <li><span>10분</span><img src={require("../../img/btn_arrow.png")} alt="arrow" /></li>
          <li><span>테마</span><img src={require("../../img/btn_arrow.png")} alt="arrow" /></li>
          <li><span>설정 초기화</span></li>
        </ul>
        <div className="mobileChartArea">차트 서비스 준비중입니다.</div>
      </div>
    );
  }
}

export default Coinchart;