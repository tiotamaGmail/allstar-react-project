import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { applyMiddleware, createStore, compose } from 'redux'
import thunk from 'redux-thunk'
import { sessionService } from 'redux-react-session'
import { logger } from 'redux-logger'
import reducer from './reducers'
import App from './App'

const isProd = process.env.NODE_ENV === 'production';

const middlewares = [thunk];

let composeEnhancers = compose;
if (!isProd) {
	middlewares.push(logger);
	composeEnhancers =
		window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const store = createStore(
  reducer,
	composeEnhancers(applyMiddleware(...middlewares))
);

// Initiate the session service
const options = {
	driver: 'COOKIES',
	expires: 1 // one day
};
sessionService.initSessionService(store, options);

let root = document.getElementById('root');

render(
  <Provider store={store}>
    <App />
  </Provider>
  , root
);

