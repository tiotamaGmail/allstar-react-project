import React from 'react';

class Krwwithdraw extends React.Component{
  render(){
    return(
        <section id="krwWithdrawCenter">
            <article id="krwWithdrawArticle1">
                <div id="krwWithdrawDiv">
                    <div class="krwWithdrawHeader">
                        <p>KRW 출금</p>
                        <p>보유중인 KRW 지갑 잔고를 은행계좌로 출금합니다.</p>
                    </div>
                    <div class="krwWithdrawBody1">
                        <p>STEP 1. 출금 금액을 입력해 주십시오.</p>
                        <div>
                            <ul>
                                <li>
                                    출금 가능
                                    <span>?</span>
                                    <div class="krwWithdrawGuide">
                                        <p>1회 출금 한도: 10,000,000 KRW</p>
                                        <p>1일 출금 잔여액/한도: 10,000,000 KRW/ 10,000,000 KRW</p>
                                        <p>고객님의 사용 가능 잔액: 0 KRW</p>
                                        <p>현재 출금 가능 금액은, 위 세가지 금액을 반영한 값입니다.</p>
                                    </div>
                                </li>
                                <li>0<span>KRW</span></li>
                            </ul>
                            <div>
                                <input type="text" value="0"/>
                                <span>KRW</span>
                                <div>
                                    <div></div>
                                    <div></div>
                                    <span>0%</span>
                                </div>
                                <ul>
                                    <li>출금 수수료</li>
                                    <li>1,000<span>KRW</span></li>
                                </ul>
                                <ul>
                                    <li>총 출금액</li>
                                    <li>0<span>KRW</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="krwWithdrawBody2">
                        <p>STEP 2. 고객님의 계좌 정보를 확인해 주십시오.</p>
                        <ul>
                            <li>은행</li>
                            <li>우리은행</li>
                        </ul>
                        <ul>
                            <li>계좌번호</li>
                            <li>00*******00</li>
                        </ul>
                        <ul>
                            <li>예금주</li>
                            <li>최강일</li>
                        </ul>
                        <div class="krwWithdrawBody2Wallet">
                            <a class="krwWithdrawOpen">지갑주소 관리</a>
                        </div>
                        <div>
                            <a>← 지갑관리</a>
                            <a>2FA 인증</a>
                        </div>
                    </div>
                    <div class="krwWithdrawBody3">
                        <p>확인해 주십시오</p>
                        <ul>
                            <li>출금 요청 완료 후, 약 5-30분 이내에 등록하신 은행계좌로 출금됩니다.</li>
                            <li>단, 은행점검시간 또는 서버 작업이 진행 중일 때에는 반영이 지체될 수 있습니다.</li>
                            <li>은행 공통 점검시간은 매일 23:00~00:30이며, 점검시간에는 출금이 제한됩니다.</li>
                            <li>부정 거래가 의심되는 경우, 출금이 제한될 수 있습니다.</li>
                            <li>출금한도는 인증 단계에 따라 차등 적용됩니다. 출금한도 상향 조정은 <a>고객지원 센터</a>에 문의해 주십시오.</li>
                            <li>출금 수수료: 1,000원 / 출금 최소 금액: 5,000원</li>
                            <li>1회 출금 최대금액: 10,000,000원</li>
                        </ul>
                    </div>
                    <div class="krwWithdrawPopup">
                        <ul>
                            <li>
                                <select>
                                    <option>KRW 지갑</option>
                                </select>
                                <span><img src="img/closeBtn.png" class="krwWithdrawClose1"/></span>
                            </li>
                            <li>
                                <p>최강일님의 KRW 계좌<span>삭제(3회 남음)</span></p>
                            </li>
                            <li>
                                <p>은행명</p>
                                <p>
                                    <input type="text"/>
                                </p>
                            </li>
                            <li>
                                <p>계좌번호</p>
                                <p>
                                    <input type="text"/>
                                </p>
                            </li>
                            <li>
                                <p>예금주</p>
                                <p>
                                    <input type="text"/>
                                </p>
                            </li>
                            <li>
                                <p>계좌 닉네임</p>
                                <p>
                                    <input type="text"/>
                                </p>
                            </li>
                            <li>
                                <div class="krwWithdrawClose2">닫기</div>
                            </li>
                        </ul>
                    </div>

                </div>
            </article>
        </section>
    );
  }
}

export default Krwwithdraw;