import * as types from '../actions/ActionTypes';
import update from 'react-addons-update';

const initialState = {
    login: {
        status: 'INIT'
    },
    register: {
        status: 'INIT',
        error: -1
    },
    favorite: {
        status: 'INIT',
        error: -1
    },
    email: {
        isSignupEmail: false
    },
    status: {
        isLoggedIn: false,
        currentUser: '',
        currentPassword: ''
    },
    coin: {
        coinList: [],
        id:0
    }
};

export default function authentication(state, action) {
    if(typeof state === "undefined")
        state = initialState;

    switch(action.type) {
        /* LOGIN */
        case types.AUTH_LOGIN:
            return update(state, {
                login: {
                    status: { $set: 'WAITING' }
                }
            });
        case types.AUTH_LOGIN_SUCCESS:
            return update(state, {
                login: {
                    status: { $set: 'SUCCESS' }
                },
                status: {
                    isLoggedIn: { $set: true },
                    currentUser: { $set: action.username },
                    currentPassword: {$set:action.password}
                }
            });
        case types.AUTH_LOGIN_FAILURE:
            return update(state, {
                login: {
                    status: { $set: 'FAILURE' }
                }
            });
        case types.AUTH_LOGOUT:
            return update(state, {
                login: {
                    status: { $set: 'INIT' }
                },
                status: {
                    isLoggedIn: { $set: false },
                    currentUser: { $set: '' },
                    currentPassword: {$set: ''}
                }
            });
        case types.AUTH_REGISTER:
            return update(state, {
                register: {
                    status: { $set: 'WAITING' },
                    error: { $set: -1 }
                }
            });
        case types.AUTH_REGISTER_SUCCESS:
            return update(state, {
                register: {
                    status: { $set: 'SUCCESS' }
                }
            });
        case types.AUTH_REGISTER_FAILURE:
            return update(state, {
                register: {
                    status: { $set: 'FAILURE' },
                    error: { $set: action.error }
                }
            });

            
            case types.AUTH_FAVORITE_SUCCESS:
            return update(state, {
                favorite: {
                    status: { $set: 'SUCCESS' }
                }
            });
        case types.AUTH_FAVORITE_FAILURE:
            return update(state, {
                favorite: {
                    status: { $set: 'FAILURE' },
                    error: { $set: action.error }
                }
            });

        case types.AUTH_SEND_EMAIL:
        case types.AUTH_RECIEVE_EMAIL:
            break;
        case types.AUTH_EMAIL_SUCCESS:
            return update(state, {
                email: {
                    isSignupEmail: { $set: true },
                }
            });
        case types.AUTH_EMAIL_FAILURE:
            return update(state, {
                email: {
                    isSignupEmail: { $set: false },
                }
            });
        case types.COINLIST_LOAD:
            return update(state, {
                coin: {
                    coinList : {$set: action.coinList }
                }
            });
        case types.COINLIST_SET_INDEX:
            return update(state, {
                coin: {
                    id : {$set: action.id }
                }
            });
        default:
            return state;
    }
}