$(document).ready(function(){
    //즐겨찾기
    $("#tradeFavorite").click(function(){
        if($(this).find(">img").attr("src")=="img/favorite.png"){
            $(this).find(">img").attr("src","img/favorite_on.png");
        }else{
            $(this).find(">img").attr("src","img/favorite.png");
        }
    });
    
    //사이드메뉴
    $(".tradeSideOpen").click(function(){
        $("#tradeMenu").css({"display":"block"});
    });
    $(".tradeSideClose").click(function(){
        $("#tradeMenu").css({"display":"none"});
    });
    
    //trade 메뉴
    $(".tradeChoose li").each(function(idx){
        $(this).click(function(){
            $(".tradeChoose li").eq(idx).css({
                "color":"#f5b74c",
                "boxShadow":"0px 1px 0px #f5b74c"
            });
            $(".tradeChoose li").not($(this)).css({
                "color":"#fff",
                "boxShadow":"none"
            });
        });
    });
    
    //trade 테이블 th
    $(".article1T th").each(function(idx){
        $(this).click(function(){
            if($(this).find(">img").attr("src")=="img/highsort.png"){
                $(this).find(">img").attr("src","img/lowsort.png");
            }else{
                $(this).find(">img").attr("src","img/highsort.png");
            }
            $(".article1T th").not($(this)).find(">img").attr("src","img/btn_index.png");
            $(".article1T th").eq(0).find(">img").attr("src","img/horizental_arrow.png");
        });
    });
    
    //wallet tr hide
    $(".walletBody1T tr:nth-child(2)").click(function(){
        if($(".walletBodyHide").css("display")=="none"){
            $(".walletBodyHide").css({"display":"table-row"});
        }else{
            $(".walletBodyHide").css({"display":"none"});
        }
    });
    
    //wallet 체크박스
    $(".walletCheckView img").click(function(){
       if($(this).attr("src")=="img/checkbox_off.png"){
           $(this).attr("src","img/checkbox_on.png");
           $(".walletCheckView input").val("on");
           $(".walletCheckView img").attr("src","img/checkbox_on.png");
       }else{
           $(this).attr("src","img/checkbox_off.png");       
           $(".walletCheckView input").val("off");
           $(".walletCheckView img").attr("src","img/checkbox_off.png");
       }
    });
    
    //wallet 테이블 th
    $(".walletBody2T th").each(function(idx){
        $(this).click(function(){
            if($(this).find(">img").attr("src")=="img/highsort.png"){
                $(this).find(">img").attr("src","img/lowsort.png");
            }else{
                $(this).find(">img").attr("src","img/highsort.png");
            }
            $(".walletBody2T th").not($(this)).find(">img").attr("src","img/btn_index.png");
        });
    });
    
    //krw_withdraw guide
    $(".krwWithdrawGuide").prev().click(function(){
        if($(".krwWithdrawGuide").css("display")=="none"){
            $(".krwWithdrawGuide").css({"display":"block"});
        }else{
            $(".krwWithdrawGuide").css({"display":"none"});
        }
    });
    
    //krw_deposit 지갑관리팝업
    $(".krwDepositOpen").click(function(){
        $(".krwDepositPopup").css({"display":"block"});
    });
    $(".krwDepositClose1").click(function(){
        $(".krwDepositPopup").css({"display":"none"});
    });
    $(".krwDepositClose2").click(function(){
        $(".krwDepositPopup").css({"display":"none"});
    });
    
    //krw_withdraw 지갑관리팝업
    $(".krwWithdrawOpen").click(function(){
        $(".krwWithdrawPopup").css({"display":"block"});
    });
    $(".krwWithdrawClose1").click(function(){
        $(".krwWithdrawPopup").css({"display":"none"});
    });
    $(".krwWithdrawClose2").click(function(){
        $(".krwWithdrawPopup").css({"display":"none"});
    });
    
    //account 계좌등록팝업
    $(".accountOpen").click(function(){
        $(".accountBodyPopup").css({"display":"block"});
    });
    $(".accountClose1").click(function(){
        $(".accountBodyPopup").css({"display":"none"});
    });
    $(".accountClose2").click(function(){
        $(".accountBodyPopup").css({"display":"none"});
    });
    $(".accountNextOpen").click(function(){
        $(".accountBodyPopup").css({"display":"none"});
        $(".accountNextPopup").css({"display":"block"});
    });
    $(".accountNextClose1").click(function(){
        $(".accountNextPopup").css({"display":"none"});
    });
    $(".accountNextClose2").click(function(){
        $(".accountNextPopup").css({"display":"none"});
    });
    
    //auth_identity 팝업
    $(".authIdentityClose").click(function(){
        $(".authIdentityPopup").css({"display":"none"});
    });
    
    //본인인증 체크박스
    $(".authSmartAgree li:nth-child(1) p img").click(function(){
       if($(this).attr("src")=="img/checkbox_off.png"){
           $(this).attr("src","img/checkbox_on.png");
           $(".authSmartAgree li:nth-child(2) p img").attr("src","img/checkbox_on.png");
           $(".authSmartAgree li:nth-child(3) p img").attr("src","img/checkbox_on.png");
           $(this).next("input").val("on");
           $(".authSmartAgree li:nth-child(2) p img").next("input").val("on");
           $(".authSmartAgree li:nth-child(3) p img").next("input").val("on");
       }else{
           $(this).attr("src","img/checkbox_off.png");
           $(".authSmartAgree li:nth-child(2) p img").attr("src","img/checkbox_off.png");
           $(".authSmartAgree li:nth-child(3) p img").attr("src","img/checkbox_off.png");
           $(this).next("input").val("off");
           $(".authSmartAgree li:nth-child(2) p img").next("input").val("off");
           $(".authSmartAgree li:nth-child(3) p img").next("input").val("off");
       }
    });
    
    $(".authSmartCheckEach1 img").click(function(){
        if($(this).attr("src")=="img/checkbox_off.png"){
            $(this).attr("src","img/checkbox_on.png");
            $(this).next("input").val("on");
        }else{
            $(this).attr("src","img/checkbox_off.png");
            $(this).next("input").val("off");
        }
    });
    $(".authSmartCheckEach2 img").click(function(){
        if($(this).attr("src")=="img/checkbox_off.png"){
            $(this).attr("src","img/checkbox_on.png");
            $(this).next("input").val("on");
        }else{
            $(this).attr("src","img/checkbox_off.png");
            $(this).next("input").val("off");
        }
    });
    
    //신원확인 체크박스
    $(".authIdentityAgree li:nth-child(1) p img").click(function(){
       if($(this).attr("src")=="img/checkbox_off.png"){
           $(this).attr("src","img/checkbox_on.png");
           $(".authIdentityAgree li:nth-child(2) p img").attr("src","img/checkbox_on.png");
           $(".authIdentityAgree li:nth-child(3) p img").attr("src","img/checkbox_on.png");
           $(this).next("input").val("on");
           $(".authIdentityAgree li:nth-child(2) p img").next("input").val("on");
           $(".authOdentityAgree li:nth-child(3) p img").next("input").val("on");
       }else{
           $(this).attr("src","img/checkbox_off.png");
           $(".authIdentityAgree li:nth-child(2) p img").attr("src","img/checkbox_off.png");
           $(".authIdentityAgree li:nth-child(3) p img").attr("src","img/checkbox_off.png");
           $(this).next("input").val("off");
           $(".authIdentityAgree li:nth-child(2) p img").next("input").val("off");
           $(".authIsentityAgree li:nth-child(3) p img").next("input").val("off");
       }
    });
    
    $(".authIdentityCheckEach1 img").click(function(){
        if($(this).attr("src")=="img/checkbox_off.png"){
            $(this).attr("src","img/checkbox_on.png");
            $(this).next("input").val("on");
        }else{
            $(this).attr("src","img/checkbox_off.png");
            $(this).next("input").val("off");
        }
    });
    $(".authIdentityCheckEach2 img").click(function(){
        if($(this).attr("src")=="img/checkbox_off.png"){
            $(this).attr("src","img/checkbox_on.png");
            $(this).next("input").val("on");
        }else{
            $(this).attr("src","img/checkbox_off.png");
            $(this).next("input").val("off");
        }
    });
    
    $(".coinHref").click(function(){
        window.location.href="trade_detail.php";
    });
    
});