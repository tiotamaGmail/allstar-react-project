import React from 'react';

class Passwordchange extends React.Component{
  render(){
    return(
      <section id="passwordChangeCenter">
            <article id="passwordChangeArticle1">
                <div class="passwordChangeDiv">
                    <p>패스워드 변경</p>
                    <p><input type="password" placeholder="기존 비밀번호" /></p>
                    <p><input type="password" placeholder="새로운 비밀번호" /></p>
                    <p><input type="password" placeholder="새로운 비밀번호 확인" /></p>
                    <p><div>패스워드 변경</div></p>
                </div>
            </article>
        </section>
    );
  }
}

export default Passwordchange;