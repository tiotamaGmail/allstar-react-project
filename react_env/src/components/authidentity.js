import React from 'react';

class Authidentity extends React.Component{
  render(){
    return(
      <section id="authIdentityCenter">
          <article id="authIdentityArticle1">
              <div id="authIdentityDiv">
                  <div className="authIdentityHeader">
                      <p>신원확인</p>
                      <p>
                          당사는 고객님의 정보를 귀중히 여기며,<br />
                          안전하게 보관될 수 있도록 최선을 다합니다.
                      </p>
                  </div>
                  <div className="authIdentityBody1">
                      <div className="authIdentity1">
                          <div>
                              <p>신분증 사본</p>
                              <p>
                                  <input type="text" placeholder="예: 주민증록증, 여권, 운전면허증" readonly />
                                  <label htmlFor="authIdentityUpload1">등록</label>
                                  <input type="file" id="authIdentityUpload1" style={{display:'none'}} />
                              </p>
                          </div>
                          <ul>
                              <li>유효한 신분증에는 <span>주민등록증, 여권, 운전면허증, 외국인등록증, 외국국적동포 거소신고증, 재외국민주민등록증</span>이 해당됩니다.</li>
                              <li>만료되지 않은 신분증을 사용해 주십시오.</li>
                              <li><span>신분증의 주민등록번호 뒷자리를 가려 주십시오.</span></li>
                              <li>본인의 얼굴이 선명하게 나온 신분증을 사용해 주십시오.</li>
                              <li>밝은 곳에서 신분증 상의 모든 정보가 선명하게 보이도록 촬영되어야 합니다.</li>
                              <li>포토샵과 같은 프로그램을 사용하여 이미지를 보정하지 마십시오.</li>
                              <li><a>예시 보기</a></li>
                          </ul>
                      </div>
                      <div className="authIdentity2">
                          <div>
                              <p>신원 확인용 사진</p>
                              <p>
                                  <input type="text" placeholder="예: 올스타빗과 오늘 날짜 메모, 신분증" readonly />
                                  <label htmlFor="authIdentityUpload2">등록</label>
                                  <input type="file" id="authIdentityUpload2" style={{display:'none'}} />
                              </p>
                          </div>
                          <ul>
                              <li>신원확인용 사진에는 아래 세 가지가 모두 포함되어야 합니다.</li>
                              <li><span>1) 자필로 올스타빗과 오늘의 날짜를 적은 메모, 2) 신분증, 3) 고객님의 얼굴</span></li>
                              <li>유효기간이 표기된 신분증을 사용해 주십시오.</li>
                              <li>밝은 곳에서 신분증과 메모 상의 모든 정보가 선명하게 보이도록 촬영되어야 합니다.</li>
                              <li><span>신분증의 주민등록번호 뒷자리만 가리고</span>, 메모와 고객님의 얼굴 전체가 확인될 수 있는 사진을 전송해 주십시오. (모자, 머플러, 안내, 선글라스 등 착용 금지)</li>
                              <li>포토샵과 같은 프로그램을 사용하여 이미지를 보정하지 마십시오.</li>
                              <li><a>예시 보기</a></li>
                          </ul>
                      </div>
                      <ul className="authIdentityAgree">
                          <li>
                              <p>
                                  <img src={require("../img/checkbox_off.png")} />
                                  <input type="checkbox" id="authIdentityCheck1" value="off" style={{display:'none'}} />
                                  <label htmlFor="authIdentityCheck1">모두 동의합니다</label>
                              </p>
                          </li>
                          <li>
                              <p>개인정보 수집 및 이용에 대한 동의<span>(필수)</span></p>
                              <textarea readonly></textarea>
                              <p className="authIdentityCheckEach1">
                                  <img src={require("../img/checkbox_off.png")} />
                                  <input type="checkbox" id="authIdentityCheck2" value="off" style={{display:'none'}} />
                                  <label htmlFor="authIdentityCheck2">모두 동의합니다</label>
                              </p>
                          </li>
                          <li>
                              <p>이벤트 등 프로모션 알림 메일 수신에 대한 동의<span>(선택)</span></p>
                              <textarea readonly></textarea>
                              <p className="authIdentityCheckEach2">
                                  <img src={require("../img/checkbox_off.png")} />
                                  <input type="checkbox" id="authIdentityCheck3" value="off" style={{display:'none'}} />
                                  <label htmlFor="authIdentityCheck3">모두 동의합니다</label>
                              </p>
                          </li>
                      </ul>
                      <div className="authIdentitySubmit">
                          <a>확인</a>
                      </div>
                      <div className="authIdentityPopup" style={{display:'none'}}>
                          <ul>
                              <li>
                                  <p>알림</p>
                              </li>
                              <li>
                                  <p>신분증과 신원 확인용 사진 촬영 전 도움말을 주의 깊게 읽어 주시고, <span>예시 사진</span>을 반드시 확인해 주시기 바랍니다.</p>
                                  <p>재촬영이 필요한 경우, 심사 시간이 추가로 소요될 수 있습니다.</p>
                              </li>
                              <li>
                                  <a className="authIdentityClose">확인</a>
                              </li>
                          </ul>
                      </div>
                  </div>
              </div>
          </article>
      </section>
    );
  }
}

export default Authidentity;