import React from 'react';

class Noticeread extends React.Component{
  render(){
    return(
      <section id="noticeReadCenter">
        <article id="article1">
          <div className="noticeReadTitle"><a href="javascript:void(0);">올스타빗 공지사항</a></div>
          <h1 className="noticeReadHead clear">[상장] 공지사항 입니다 정책 변경 안내</h1>
          <div className="noticeReadContent">
            안녕하세요.
            암호화폐거래소 올스타빗입니다.<br /><br />
            현재 일부 고객님들의 거래기록 페이지 내 상세 내역 및 거래수수료가 올바르게 표기되지 않는 현상이 일어나고 있습니다.
            이와 같은 현상을 겪으시는 고객님들의 경우 거래소 화면 내 '체결완료내역' 역시 표기 오류 현상이 발생할 수 있으니 이용에 참고하시기 바랍니다.<br /><br />
            해당 오류는 단순 표기 오류일 뿐, 수수료 계산은 정상적으로 이루어지고 있으며 
            고객님의 자산에는 영향이 전혀 없음을 안내드립니다.<br /><br />
            최대한 빠른 시간 내에 정상화 한 후 공지를 통해 알려드리겠습니다.<br /><br />
            이용에 불편을 드려 죄송합니다.<br /><br />
            올스타빗 드림<br /><br />
          </div>
        </article>
      </section>
    );
  }
}

export default Noticeread;