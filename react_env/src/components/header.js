import React, { Component } from 'react';
import {Link} from "react-router-dom";
import { connect } from 'react-redux';
import { logoutRequest } from '../actions/authentication';

const mapStateToProps = (state) => {
  return {
  	isLogin: state.session.authenticated,
		// status: state.authentication.login.status,
		// isLogin:state.authentication.status.isLoggedIn,
		// user:state.authentication.status.currentUser,
		// password:state.authentication.status.currentPassword
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    logout: () => {
			return dispatch(logoutRequest());
		}
  };
};

class Header extends Component{
  constructor(props) {
    super(props);
    this.state = {isToggleOn: false};
  }

  render(){
    const login = () => {
      if(this.props.isLogin)
      {
        return(
          <span onClick={() => this.props.logout()}>
           <a>로그아웃</a>
          </span>
        );
      }
      else{
        return(
          <Link onClick={() => this.setState({isToggleOn: false})} to="/login">
            <a>로그인을 해주세요</a>
          </Link>
        );
      }
    };
    return(
      <header>
        <div><Link to="/exchangemain"><img src={require("../img/allstarbit_m.png")} alt="allstarbit" /></Link></div>
        <div onClick={() => this.setState({isToggleOn: true})}><img src={require("../img/menu_m.png")} alt="allstarbit" className="tradeSideOpen"/></div>
        {
          this.state.isToggleOn ?
          <div id="tradeMenu">
            <div>
              <div>
                {login()}
                <img src={require("../img/closeBtn.png")} alt="Close" className="tradeSideClose" onClick={() => this.setState({isToggleOn: false})}/>
              </div>
              <ul>
                <li><Link onClick={() => this.setState({isToggleOn: false})} to="/"><img src={require("../img/m_tradephp.png")} alt="allstarbit" /><span>거래소</span></Link></li>
                <li><Link onClick={() => this.setState({isToggleOn: false})} to={this.props.isLogin?"/wallet":"/login"}><img src={require("../img/m_walletphp.png")} alt="allstarbit" /><span>지갑관리</span></Link></li>
                <li><Link onClick={() => this.setState({isToggleOn: false})} to={this.props.isLogin?"/investment":"/login"}><img src={require("../img/m_investmentphp.png")} alt="allstarbit" /><span>투자내역</span></Link></li>
              </ul>
              <ul>
                <li><Link onClick={() => this.setState({isToggleOn: false})} to="/account">계정관리</Link></li>
                <li><Link onClick={() => this.setState({isToggleOn: false})} to="/notice">공지사항</Link></li>
              </ul>
            </div>
          </div>
        : null
        }
      </header>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
