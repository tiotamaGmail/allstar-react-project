import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {Route, HashRouter} from "react-router-dom";

import PrivateRoute from './components/PrivateRoute';

// import components
import Header from './components/header';
import Trade from './components/trade';
import Tradedetails from './components/tradedetails';
import Wallet from './components/wallet';
import Investment from './components/investment';
import Notice from './components/notice';
import Account from './components/account';
import Footer from './components/footer';
import Login from './components/login';
import Register from './components/register';
import Authotp from './components/authotp';
import Passwordset from './components/passwordset';
import Authsmart from './components/authsmart';
import Krwdeposit from './components/krwdeposit';
import Krwwithdraw from './components/krwwithdraw';
import Exchangemain from './components/exchangemain';
import Passwordchange from './components/passwordchange';

const App = ({ authenticated, checked }) => (
	<HashRouter>
		{checked &&
		<div className="App">
			<Header/>
			<div className="content">
				<Route exact path="/" component={Trade}/>
				<Route path="/tradedetails/:target?/:source?" component={Tradedetails}/>
				<Route path="/notice" component={Notice}/>
				<Route path="/login" component={Login}/>
				<Route path="/register" component={Register}/>
				<Route path="/authotp" component={Authotp}/>
				<Route path="/passwordset" component={Passwordset}/>
				<Route path="/Authsmart" component={Authsmart}/>
				<Route path="/Krwdeposit" component={Krwdeposit}/>
				<Route path="/Krwwithdraw" component={Krwwithdraw}/>
				<Route path="/exchangemain" component={Exchangemain}/>
				<Route path="/passwordchange" component={Passwordchange}/>
				<PrivateRoute
					exact
					path="/investment"
					redirect="/login"
					component={Investment}
					authenticated={authenticated}/>
				<PrivateRoute
					exact
					path="/wallet"
					redirect="/login"
					component={Wallet}
					authenticated={authenticated}/>
				<PrivateRoute
					exact
					path="/account"
					redirect="/login"
					component={Account}
					authenticated={authenticated}/>
			</div>
		</div>
		}
	</HashRouter>
);

const { bool } = PropTypes;

App.propTypes = {
	authenticated: bool.isRequired,
	checked: bool.isRequired
};

const mapState = ({ session }) => ({
	checked: session.checked,
	authenticated: session.authenticated
});

export default connect(mapState)(App);
