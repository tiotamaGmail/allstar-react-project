import React from 'react';

class Footer extends React.Component{
  render(){
    return(
      <footer>
        <div>
          <div><img src={require("../img/allstarbit_logo.png")} alt="allstarbit" /></div>
          <div>
              <p>
                <b>올스타빗 암호화폐거래소</b>
              </p>
              <p>인천광역시 서구 담지로 8번길 7-14, 1층(연희동)</p>
              <p>사업자 등록번호 : 489-81-00823</p>
              <p>통신판매업신고 : 2018-인천서구-0450호</p>
              <br/>
              <p>문의 : allstarinc@allstar-m.com &nbsp;| &nbsp; 1670-3512</p>
              <p>시간 : 평일 09:00 ~ 18:00(점심시간 12:30 ~ 13:30)</p>
              <p>고객 상담은 전화, 채팅, 이메일로만 진행하며, 방문상담은 진행하지 않습니다</p>
              <br/>
              <br/>
              <p>COPYRIGHT 2018ⓒALLSTARBIT ALL RIGHT RESERVED.</p>
              <br/>
              <br/>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;