import React from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom";
import { connect } from 'react-redux';
import axios from 'axios';

class Account extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      isToggleOn: false,
      userInfo:{}
    };
    
  }
  componentDidMount(){
    let data = {
      'REQ': 'user_read',
      'email':this.props.email,
      'password':this.props.password
    };

    return axios.post('http://api.allstarbit.com/req.php', data).then((response)=>{
    	console.log(response);
      // SUCCEED
      this.setState({userInfo:response.data.result || {}});
    }).catch((error)=>{
      // FAILED
      console.log(error);
    });

  }
  render(){
    let {userInfo} = this.state;
    
    console.log(userInfo);
    return(
      <section id="accountCenter">
        <article id="accountArticle1">
          <div id="accountDiv">
            <div className="accountHeader">
              {/* <p>{userInfo.email}</p> */}
              <div>
                <Link to="/passwordchange">패스워드 변경</Link>
                <a>지갑주소 관리</a>
              </div>
            </div>
            <div className="accountBody1">
              <p>올스타빗 인증 과정</p>
              <ul>
                <li>
                  <a>
                    <p><img src={userInfo.level>0?require("../img/step_on.png"):require("../img/step_off.png")} alt="Off" /></p>
                    <p><img src={require("../img/step_email.png")} alt="Email" /></p>
                    <p>이메일인증</p>
                    <p>완료</p>
                    <p></p>
                  </a>
                </li>
                <li>
                  <Link to="/Authsmart">
                    <p><img src={userInfo.level>1?require("../img/step_on.png"):require("../img/step_off.png")} alt="Off" /></p>
                    <p><img src={require("../img/step_smart.png")} alt="Smart" /></p>
                    <p>본인확인</p>
                    <p>가상화폐 입금가능</p>
                    <p>입출금한도 1000만원</p>
                  </Link>
                </li>
                <li>
                  <Link to="/authotp">
                    <p><img src={require("../img/step_off.png")} alt="Off" /></p>
                    <p><img src={require("../img/step_otp.png")} alt="Otp" /></p>
                    <p>OTP인증</p>
                    <p>가상화폐 출금가능</p>
                    <p>일 출금한도 1000만원</p>
                  </Link>
                </li>
                <li>
                  <a className="accountOpen">
                    <p><img src={userInfo.level>2?require("../img/step_on.png"):require("../img/step_off.png")} alt="Off" /></p>
                    <p><img src={require("../img/step_account.png")} alt="Account" /></p>
                    <p>계좌등록</p>
                    <p>원화 입출금가능</p>
                    <p>일 출금한도 1000만원</p>
                  </a>
                </li>
                <li>
                  <a>
                    <p><img src={userInfo.level>3?require("../img/step_on.png"):require("../img/step_off.png")} alt="Off" /></p>
                    <p><img src={require("../img/step_identity.png")} alt="Identity" /></p>
                    <p>신원확인</p>
                    <p>출금가능금액 상향</p>
                    <p>일 출금한도 1000만원</p>
                  </a>
                </li>
              </ul>
            </div>
            <div className="accountBody2">
              <ul>
                <li>회원정보</li>
                <li>
                  <p>닉네임</p>
                  <p><input type="text" /><span>수정</span></p>
                </li>
                <li>
                  <p>회원등급</p>
                  <p><input type="text" /></p>
                </li>
              </ul>
              <ul>
                <li>연락처정보</li>
                <li>
                  <p>이메일/아이디</p>
                  <p><input type="text" value={userInfo.email} readonly="readonly"/><span>수정</span></p>
                </li>
                <li></li>
              </ul>
              <ul>
                <li>보안 정보</li>
                <li>
                  <p>2FA</p>
                  <p><input type="text" /><span>비활성화</span></p>
                </li>
              </ul>
              <ul>
                <li>접속 기록</li>
                <li>
                  <div>
                    <div>
                      <table className="accountBody1T">
                        <tbody>
                          <tr>
                            <th>로그인시각</th>
                            <th>IP주소</th>
                            <th>위치</th>
                            <th>기기</th>
                          </tr>
                          <tr>
                            <td>18.03.27 15:32:43</td>
                            <td>144.444.280.929</td>
                            <td>Korea,Republic of</td>
                            <td>windows.chrome</td>
                          </tr>
                          <tr>
                            <td>18.03.27 15:32:43</td>
                            <td>144.444.280.929</td>
                            <td>Korea,Republic of</td>
                            <td>windows.chrome</td>
                          </tr>
                          <tr>
                            <td>18.03.27 15:32:43</td>
                            <td>144.444.280.929</td>
                            <td>Korea,Republic of</td>
                            <td>windows.chrome</td>
                          </tr>
                          <tr>
                            <td>18.03.27 15:32:43</td>
                            <td>144.444.280.929</td>
                            <td>Korea,Republic of</td>
                            <td>windows.chrome</td>
                          </tr>
                          <tr>
                            <td>18.03.27 15:32:43</td>
                            <td>144.444.280.929</td>
                            <td>Korea,Republic of</td>
                            <td>windows.chrome</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </li>
              </ul>
              <ul>
                <li>회원탈퇴</li>
                <li>
                  <p>회원 탈퇴 시, 올스타빗에서 보유하고 있는 고객님의 개인정보는 삭제, 폐기되며 복구되지 않습니다. 신중히 결정해주십시오.</p>
                  <p>※고객님의 계정에 잔액이 남아있을 시, 탈퇴 절차가 정상적으로 진행되지 않습니다. 잔액을 모두 출금하신 후 요청해 주세요.</p>
                </li>
                <li><a>회원탈퇴<span style={{marginLeft:'15px',color:'#888'}}>서비스 준비중입니다.</span></a></li>
              </ul>
            </div>
            <div className="accountBodyPopup">
              <ul>
                <li>
                  <select><option>KRW 지갑</option></select>
                  <span><img src={require("../img/closeBtn.png")} className="accountClose1" alt="Close" /></span>
                </li>
                <li>
                  <p>
                      아직 등록된 은행 계좌정보가 없습니다<br />
                      입출금에 사용할 고객님의 은행 계좌정보를 입력해 주십시오.
                  </p>
                </li>
                <li>
                  <select>
                    <option>은행 선택</option>
                    <option>KB국민은행</option>
                    <option>IBK기업은행</option>
                    <option>우리은행</option>
                  </select>
                </li>
                <li>
                  <input type="text" placeholder="계좌번호" />
                </li>
                <li>
                  <input type="text" placeholder="예금주" />
                </li>
                <li>
                  <input type="text" placeholder="계좌 닉네임" />
                </li>
                <li>
                  <p>
                    <a className="accountClose2">닫기</a>
                    <a className="accountNextOpen">다음</a>
                  </p>
                </li>
              </ul>
            </div>
            <div className="accountNextPopup">
              <ul>
                <li>
                  <select><option>KRW 지갑</option></select>
                  <span><img src={require("../img/closeBtn.png")} className="accountNextClose1" alt="Close" /></span>
                </li>
                <li>
                  <p>등록된 계좌로 받으신 입금 정보(1원)에서 고유코드 [ALLSTAR***]를 확인하신 후 아래에 숫자 3자리를 입력해 주십시오.</p>
                </li>
                <li>
                  <input type="text" placeholder="Secret Code" />
                </li>
                <li>
                  <p><img src={require("../img/warning.png")} alt="Warning" /></p>
                  <p>고유코드를 수신하지 못하셨다면 <a>고객지원센터</a>로 문의해 주십시오.</p>
                </li>
                <li>
                  <p>
                    <a className="accountNextClose2">닫기</a>
                    <a>인증</a>
                  </p>
                </li>
              </ul>
            </div>
          </div>
        </article>
      </section>
    );
  }
}

const { string } = PropTypes;
Account.propTypes = {
	email: string.isRequired,
	password: string.isRequired
};

const mapState = ({ session }) => ({
	email: session.user.email,
	password: session.user.password
});

export default connect(mapState)(Account);
