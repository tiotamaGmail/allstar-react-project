<?php include "header.php"; ?>

        <section id="pwSetCenter">
            <article id="pwSetArticle1">
                <div class="pwSetDiv">
                    <p>패스워드 재설정</p>
                    <p>이메일을 입력해 주십시오. 입력하신 이메일로 이메일 인증코드가 발송 됩니다.</p>
                    <p><input type="text" placeholder="이메일 입력"></p>
                    <p><a>이메일 인증코드 발송</a></p>
                </div>
            </article>
        </section>
        
<?php include "footer.php"; ?>