<?php include "header.php"; ?>

        <section id="loginCenter">
            <article id="loginArticle1">
                <div class="loginDiv">
                    <div>로그인</div>
                    <div>
                        <input type="email" placeholder="이메일 입력">
                        <input type="password" placeholder="비밀번호 입력">
                        <div>로그인</div>
                    </div>
                    <div>
                        <p>아직 올스타빗 계정이 없으신가요?<a href="register.php">회원가입</a></p>
                        <p>로그인에 문제가 있으신가요?<a href="passwordSet.php">패스워드 재설정</a></p>
                    </div>
                </div>
            </article>
        </section>
        
<?php include "footer.php"; ?>