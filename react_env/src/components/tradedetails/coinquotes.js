import React from 'react';
import axios from 'axios';
import Table from 'rc-table';

const dailyColumns = [
  { title: '일자', dataIndex: 'tradeDate', key: 'tradeDate'},
  { title: '종가(KRW)', dataIndex: 'prevClosingPrice', key: 'prevClosingPrice'},
  { title: '전일대비', dataIndex: 'changePrice', key: 'changePrice'},
  { title: '거래량(BTC)', dataIndex: 'accTradeVolume', key: 'accTradeVolume'},
];

const realTimeColumns = [
  { title: '체결시간', dataIndex: 'tradeTime', key: 'tradeDate'},
  { title: '체결가격(KRW)', dataIndex: 'tradePrice', key: 'tradePrice'},
  { title: '거래량(BTC)', dataIndex: 'tradeVolume', key: 'tradeVolume'},
];

class Coinquotes extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      target: this.props.target,
      source: this.props.source,
      limit: 100,
      active: 'realTime',
      dailyInfoList: [],
      realtimeInfoList: []
    };

    this.timer = 0;
    this.tick = this.tick.bind(this);
    this.uploadData = this.uploadData.bind(this);
    this.changeReailTime = this.changeReailTime.bind(this);
    this.changeDaily = this.changeDaily.bind(this);

    this.uploadData(this.state.source, this.state.target, this.state.limit);
  }

  componentDidMount(){
    this.timer = setInterval(this.tick, 1000);
  }

  componentWillUnmount(){
    clearInterval(this.timer);
  }

  tick(){
    this.uploadData(this.state.source, this.state.target, this.state.limit);
  }

  uploadData(source, target, limit){
    if(this.state.active === 'daily') {
      axios.get('http://view.allstarbit.com/view2/dailyOverview.php?source='+source+'&target='+target+'&limit='+limit)
      .then(response => {
        if(response.data.length === limit)
        {
          var dailyInfoList = response.data.map(dailyInfo=>{
            return dailyInfo;
          });
          this.setState({dailyInfoList:dailyInfoList, realtimeInfoList:[]});
        }
        else {
          this.setState({dailyInfoList:[]});
        }
      });
    }
    else if(this.state.active === 'realTime') {
      axios.get('http://view.allstarbit.com/view2/transaction_list.php?source='+source+'&target='+target+'&limit='+limit)
      .then(response => {
        if(response.data.length ===limit)
        {
          var realtimeInfoList = response.data.map(dailyInfo=>{
            return dailyInfo;
          });
          this.setState({realtimeInfoList:realtimeInfoList, dailyInfoList:[]});
        }
        else {
          this.setState({realtimeInfoList:[]});
        }
      });
    }
  }

  changeReailTime() {
    this.setState({active:'realTime'});

  }

  changeDaily() {
    this.setState({active:'daily'});
  }
  
  render(){
    const quotesTable = (this.state.active === 'realTime') ? (
      (this.state.realtimeInfoList.length !== 0)?
      <Table className="mobileQuoteBody" columns={realTimeColumns} data={this.state.realtimeInfoList} />:null
    ) : (
      (this.state.dailyInfoList.length !== 0)?
      <Table className="mobileQuoteBody" columns={dailyColumns} data={this.state.dailyInfoList} />:null
    );
    const selectStyle = {
      color : '#ffa500',
      border : '1px solid #ffa500'
    }
  
    const unSelectStyle = {
      color : '#fff',
      border : '1px solid #fff'
    }
    const buttonStyle1 = (this.state.active === 'realTime') ? selectStyle : unSelectStyle;
    const buttonStyle2 = (this.state.active === 'daily') ? selectStyle : unSelectStyle;
    return(
      <div className="tradeDetailBody3">
        <div className="mobileQuote">
          <ul className="mobileQuoteHead clear">
            <li onClick={() => this.changeReailTime()} style={buttonStyle1}>체결</li>
            <li onClick={() => this.changeDaily()} style={buttonStyle2}>일별</li>
          </ul>
          <div className="mobileQuoteWrap">
            {quotesTable}
          </div>
        </div>
      </div>
    );
  }
}

export default Coinquotes;