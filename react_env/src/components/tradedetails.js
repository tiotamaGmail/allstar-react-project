import React from 'react';
import axios from 'axios';
import Tabs, { Tab } from 'react-awesome-tabs';
import { connect } from 'react-redux';
import { setCoinIndex,favoriteRequest } from '../actions/authentication';

import Coinarc from './tradedetails/coinarc';
import CoinInfo from './tradedetails/coininfo';
import Coinchart from './tradedetails/coinchart';
import Coinorder from './tradedetails/coinorder';
import Coinquotes from './tradedetails/coinquotes';
import {Link} from "react-router-dom";


const mapStateToProps = (state) => {
  if(state.session.authenticated)
  {
    return {
      isLogin:state.session.authenticated,
      user:state.session.user.email,
      password:state.session.user.password,
      coinList: state.authentication.coin.coinList,
      id: state.authentication.coin.id
    };
  }
  return {
      user:state.authentication.status.currentUser,
      password:state.authentication.status.currentPassword,
      isLogin:state.authentication.status.isLoggedIn,
      coinList: state.authentication.coin.coinList,
      id: state.authentication.coin.id
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    favoriteRequest: (id,pw,coinInfo) => { 
      return dispatch(favoriteRequest(id,pw,coinInfo)); 
    },
    setCoinIndex: (id) => {
      return dispatch(setCoinIndex(id));
    }
  };
};

class Tradedetails extends React.Component{

  constructor(props){
    super(props);
    var userData = {
      isLogin : this.props.isLogin,
      user : this.props.user,
      password : this.props.password
    };

    this.update = false;
    if(this.props.coinList && this.props.coinList.length === 0)
      this.moveIcon = false;
    else
      this.moveIcon = true;

    this.state = {
      target : this.props.match.params.target,
      source : this.props.match.params.source,
      id : this.props.id,
      userData:userData,
      curTradePrice : 0,
      overviewData : {},
      activeTabNew: 0,
      url : false
    };
    this.timer = 0;
    this.tick = this.tick.bind(this);
    this.uploadData = this.uploadData.bind(this);
    this.changeIcon = this.changeIcon.bind(this);
    this.uploadData(this.props.match.params.source, this.props.match.params.target);
  }

  componentDidMount(){
    this.timer = setInterval(this.tick, 500);
  }

  componentWillUnmount(){
    clearInterval(this.timer);
  }

  shouldComponentUpdate(nextProps, nextState){
    if(this.update){
      console.log("update");
      this.update = false;
      return true;
    }
    else {
      console.log("Not update");
      return false;
    }
  }

  tick(){
    this.uploadData(this.state.source, this.state.target);
  }

  uploadData(source, target){
    axios.get('http://view.allstarbit.com/view2/overview.php?source='+source)
    .then(response => {
      if(response.data) {
        // console.log(response.data);
        var overview = response.data.find(function(element) {
          if(element.target === target && element.source === source){
            return element;
          } 
        });
        var tradePriceNum  = Number(overview.data.tradePrice.replace(/[^0-9\.]/g,''));
        var gap = tradePriceNum - Number(overview.data.prevClosingPrice);
        if(overview.source === 'KRW'){
          if(tradePriceNum < 100)
            gap = Number(gap).toFixed(1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          else
            gap = Number(gap).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        else {
          gap = Number(gap).toFixed(8);
        }
        var overviewData = {
          koreanName : overview.data.koreanName,
          tradePrice : overview.data.tradePrice,
          gap :gap,
          rate : overview.data.prevDayPerc
        }
        
        this.setState({
          curTradePrice: overview.data.tradePrice,
          overviewData : overviewData,
        }, () => {this.update=true;});
      }
    });
  }

  handleTabSwitchNew(active) {
		this.setState({ activeTabNew: active },() => {this.update=false;});
  }

  changePrevCoin(){
    this.update=false;
    if(this.state.id !== 0){
      this.setState({
        target : this.props.coinList[this.state.id-1].target,
        source : this.props.coinList[this.state.id-1].source,
        id : this.state.id-1
      },() => {this.update=false;});
    }
  }

  changeNextCoin() {
    this.update=false;
    if(this.state.id !== this.props.coinList.length){
      this.setState({
        target : this.props.coinList[this.state.id+1].target,
        source : this.props.coinList[this.state.id+1].source,
        id : this.state.id+1
      },() => {this.update=false;});
    }
  }
  
  changeIcon = event =>
  {
   // this.setState({url:!this.state.url});
    console.log(this.props.user);
    console.log(this.props.password)
    console.log(this.state.source)
    if(this.props.user && this.props.password)
    {
      return this.props.favoriteRequest(this.props.user,this.props.password,this.state.source).then(
        () => {
        })
    }
  }
  render(){
    const plusStyle = {
      color: '#f5b74c'
    };
    const minusStyle = {
      color: '#3A86ED'
    };
    let {overviewData, target, source} = this.state;
    

    return(
      <section id="tradeDetailCenter">
        <article id="article1">
          <div className="tradeDetailHead">

            <div class="tradeDetailHeadFix">
              <h1 className="clear">
              <Link  to="/"><img src={require("../img/back1.png")} alt="back" />{overviewData.koreanName}({target}/{source})</Link>
              <img src={require("../img/notificate.png")} alt="notificate" />
              {/* <img src={require("../img/mark_off.png")} alt="off" /> */}
              {this.state.url?
                  <img src={require("../img/mark_on.png")} alt="Off" onClick={this.changeIcon.bind(this)} />
                  :<img src={require("../img/mark_off.png")} alt="Off" onClick={this.changeIcon.bind(this)} />}
              </h1>
              <p className="tradeDetailCoinPrice clear" style={overviewData.rate>0? plusStyle: minusStyle}>
              {this.moveIcon?<img src={require("../img/prev.png")} alt="prev" onClick={this.changePrevCoin.bind(this)} /> : null}{overviewData.tradePrice} {source}
              {/* <img src={require("../img/graph.png")} alt="graph" /> */}
              {this.moveIcon?<img src={require("../img/next.png")} alt="next" onClick={this.changeNextCoin.bind(this)} />: null}
              </p>
              <p className="tradeDetailCoinYesterday">
                <span>전일대비</span>
                <span style={overviewData.rate>0? plusStyle: minusStyle}>{overviewData.rate}%</span>
                <span style={overviewData.rate>0? plusStyle: minusStyle}>{(overviewData.rate>0)?"▲":"▼"}{overviewData.gap}</span>
              </p>
              <ul className="otherTradeData clear">
                <li>
                  <span>BITFINEX</span>
                  <span>0</span>
                </li>
                <li>
                  <span>BITFLYER</span>
                  <span>0</span>
                </li>
              </ul>
            </div>

            <Tabs	active={ this.state.activeTabNew }	onTabSwitch={ this.handleTabSwitchNew.bind(this) }>
              <Tab title="주문">
                <Coinorder source={this.state.source} target={this.state.target} curTradePrice={this.state.curTradePrice} userData={this.state.userData}/>
              </Tab>
              <Tab title="호가">
                <Coinarc source={this.state.source} target={this.state.target} curTradePrice={this.state.curTradePrice}/>
              </Tab>
              <Tab title="시세">
                <Coinquotes source={this.state.source} target={this.state.target}/>
              </Tab>
              <Tab title="차트">
                <Coinchart source={this.state.source} target={this.state.target}/> 
              </Tab>
              <Tab title="정보">
                <CoinInfo  target={this.state.target} source={this.state.source}/>
              </Tab>
            </Tabs>
          </div>
        </article>
      </section>
    );
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Tradedetails);