<?php include "header.php"; ?>

        <section id="registerCenter">
            <article id="article1">
            <form>
                <div class="register0">
                    <div class="register0Title">
                        회원가입
                    </div>

                    <p class="register0Text"><input type="email" placeholder="이메일 입력"></p>
                    <p class="register0Text"><input type="password" placeholder="패스워드 입력"></p>
                    <p class="register0Text"><input type="password" placeholder="패스워드 확인"></p>
                </div>
                <div class="register1 clear">
                    <div class="regisert0Allagree">
                        <input type="checkbox" style="display:none"; value="off">
                        <img src="img/checkbox_off.png">
                        <span>모두 동의 합니다</span>
                    </div>
                    <div class="register0Agree clear">
                        <p class="register0AgreeTitle">
                            <span>올스타빗 이용약관 동의</span>
                            <span>(필수)</span>
                        </p>
                            <textarea>dd</textarea>
                        <p class="register0AgreeCheck">
                            <input type="checkbox" style="display:none" value="off">
                            <img src="img/checkbox_off.png">
                            <span>동의 합니다</span>
                        </p>
                    </div>
                    <div class="register0Agree clear">
                        <p class="register0AgreeTitle">
                            <span>올스타빗 이용약관 동의</span>
                            <span>(필수)</span>
                        </p>
                            <textarea>dd</textarea>
                        <p class="register0AgreeCheck">
                            <input type="checkbox" style="display:none" value="off">
                            <img src="img/checkbox_off.png">
                            <span>동의 합니다</span>
                        </p>
                    </div>
                    <div class="register0Agree clear">
                        <p class="register0AgreeTitle">
                            <span>올스타빗 이용약관 동의</span>
                            <span>(필수)</span>
                        </p>
                            <textarea>dd</textarea>
                        <p class="register0AgreeCheck">
                            <input type="checkbox" style="display:none" value="off">
                            <img src="img/checkbox_off.png">
                            <span>동의 합니다</span>
                        </p>
                    </div>
                    <div class="register_submit">회원가입</div>
                </div>
            </form>
            <p class="registerLoginAnchor">
                <span>이미 계정이 있으신가요?<a href="login.php">로그인</a></span>
            </p>
            </article>
        </section>

        <div class="registerEmailPopup">
            <div class="registerEmailPopupIn">
                <div class="registerEmailPopupTitle">이메일 인증</div>
                <div class="registerEmailPopupContent">
                    등록해 주셔서 감사합니다 ! 
                    인증코드가 담긴 이메일을 고객님의 이메일 주소로 발송했습니다. 
                    발송된 이메일에 포함된 인증코드를 아래칸에 입력해 주십시오.
                </div>
                <div class="registerEmailPopupTitle">이메일 주소</div>
                <p class="registerEmailPopupInput"><input type="email" placeholder="받아온 이메일"></p>
                <p class="registerEmailPopupInput"><input type="text" placeholder="인증코드 입력"></p>
                <div class="registerEmailCodeResubmit clear">
                    <div>
                        <img src="img/warning.png">
                        인증코드가 발송되지 않았거나 인증 코드가 만료된 경우에는 
                        재발급 신청을 해주시기 바랍니다.
                    </div>
                    <div>인증코드 재전송</div>
                </div>
                <div class="registerEmailPopupClose">닫기</div>
                <div class="registerEmailPopupSubmit">인증코드 확인</div>
            </div>
        </div>
        
<?php include "footer.php"; ?>