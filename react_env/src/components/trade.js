import React from 'react';
import axios from 'axios';
import Trandtabs from './tradetab/tradetab';
import {withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import { coinListRequest,setCoinIndex } from '../actions/authentication';

const mapStateToProps = (state) => {
  return {
      id: state.authentication.login.status
  };
};


const mapDispatchToProps = (dispatch) => {
  return {
    coinListRequest: (source, order, sort) => {
      return dispatch(coinListRequest(source, order, sort));
    },
    setCoinIndex: (id) => {
      return dispatch(setCoinIndex(id));
    }
  };
};

class Trade extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      active: 'KRW',
      lang: 'ko',
      order : 'prevDayPerc',
      sort : 'desc',
      curId : 0,
      coinList: [],
      searchText:"",
      hasFetched:false
    };

    this.timer = 0;
    this.tick = this.tick.bind(this);
    this.uploadData = this.uploadData.bind(this);
    this.bitCoinHandleClick = this.bitCoinHandleClick.bind(this);
    this.onTabHandle = this.onTabHandle.bind(this);
    this.changeLanguage = this.changeLanguage.bind(this);
    this.makeCoinList = this.makeCoinList.bind(this);
    this.update = false;
  }

  componentDidMount(){
    this.timer = setInterval(this.tick, 500);
    this.uploadData(this.state.active, this.state.order, this.state.sort);
    this.makeCoinList();
  }

  componentWillUnmount(){
    clearInterval(this.timer);
  }

  shouldComponentUpdate(nextProps, nextState){
    if(this.update){
      console.log("update");
      this.update = false;
      return true;
    }
    else {
      console.log("Not update");
      return false;
    }
  }

  tick(){
    if(this.state.active !== 'KRW2' && this.state.hasFetched==false)
      this.uploadData(this.state.active, this.state.order, this.state.sort);
  }

  uploadData(source, order, sort){
    axios.get('http://view.allstarbit.com/view2/overview.php?source='+source+'&order='+order+'&sort='+sort)
    .then(response => {
      let idx = 0;
      var coin = response.data.map(bitcoin=>{
        if(bitcoin.data){
          let acc = Number(bitcoin.data.accTradePrice24h.replace(/[^0-9\.]/g,''))
          if(acc > 1000000)
            acc = acc / 1000000;
          let accStr = Number(acc).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

          return {
            id:idx++,
            target: bitcoin.target,
            source: bitcoin.source,
            targetEng : bitcoin.data.englishName,
            targetKo : bitcoin.data.koreanName,
            tradePrice : bitcoin.data.tradePrice,
            accTradePrice24h  : (this.state.active === 'KRW')?accStr:bitcoin.data.accTradePrice24h,
            krw : bitcoin.data.krw,
            rate : bitcoin.data.prevDayPerc
          }
        }
      });
      this.setState({coinList:coin},() => {this.update=true;});
    });
  }

  makeCoinList(){
    this.props.coinListRequest(this.state.active, this.state.order, this.state.sort);
  }

  bitCoinHandleClick(data) {
    this.setState({curId : data.id});
    this.props.setCoinIndex(data.id);
    this.props.history.push('/tradedetails/'+data.target+'/'+data.source);
  }

  onTabHandle(active){
    this.update = false;
    this.setState({active: active,hasFetched:false});
    this.props.coinListRequest(this.state.active, this.state.order, this.state.sort);
    this.makeCoinList();
  }

  sortTable(order){
    if(this.state.sort === 'desc')
      this.setState({
        order: order,
        sort:'asc'});
    else if(this.state.sort === 'asc')
      this.setState({
        order: order,
        sort:'desc'});
    this.makeCoinList();
  }
  
  onSearchText(event)
  {
    this.setState({hasFetched:true});
    axios.get('http://view.allstarbit.com/view2/overview.php?source='+this.state.active+'&keyword='+event.target.value)
    .then(response => {
      let idx = 0;
      if(response.data){
        var coin = response.data.map(bitcoin=>{
          if(bitcoin.data){
            let acc = Number(bitcoin.data.accTradePrice24h.replace(/[^0-9\.]/g,''))
            if(acc > 1000000)
              acc = acc / 1000000;
            let accStr = Number(acc).toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
           
            return {
              id:idx++,
              target: bitcoin.target,
              source: bitcoin.source,
              targetEng : bitcoin.data.englishName,
              targetKo : bitcoin.data.koreanName,
              tradePrice : bitcoin.data.tradePrice,
              accTradePrice24h  : (this.state.active === 'KRW')?accStr:bitcoin.data.accTradePrice24h,
              krw : bitcoin.data.krw,
              rate : bitcoin.data.prevDayPerc
            }
          }
        });
        this.setState({coinList:coin},() => {this.update=true;});
      }
    })
  }
  changeLanguage() {
    if(this.state.lang === 'ko')
      this.setState({lang:'en'});
    else
      this.setState({lang:'ko'})
  }

  render(){
    const plusStyle = {
      color: '#f5b74c'
    };
    const minusStyle = {
      color: '#3A86ED'
    };
    
    return(
      <section id="tradeCenter">
        <article id="article1">
          <div className="tradeSearch">
            <input type="text" onKeyUp={this.onSearchText.bind(this)} placeholder="코인명/심볼검색" />
            <div id="tradeFavorite"><img src={require("../img/favorite.png")} alt="allstarbit" />즐겨찾기</div>
          </div>

          <div className="tradeChoose">
            <Trandtabs active={this.state.active} onChange={active => {this.onTabHandle(active)}} >
              <div key="KRW">원화거래</div>
              <div key="btc">BTC</div>
              <div key="ETH">ETH</div>
              <div key="USDT">USDT</div>
            </Trandtabs>
            
            <div className="tradeContent">
              <table className="article1T">
                <thead className="article1THead clear">
                    <tr>
                      <th onClick={() => this.changeLanguage()}>한글명 <img src={require("../img/horizental_arrow.png")} alt="arrow" /></th>
                      <th onClick={tradePrice => {this.sortTable('tradePrice')}}>현재가 <img src={require("../img/btn_index.png")} alt="arrow" /></th>
                      <th onClick={prevClosingPrice => this.sortTable('prevDayPerc')}>전일대비 <img src={require("../img/btn_index.png")} alt="arrow" /></th>
                      <th onClick={accTradePrice24h => this.sortTable('accTradePrice24h')}> 거래대금 <img src={require("../img/btn_index.png")} alt="arrow" /></th>
                    </tr>
                </thead>
                <tbody className="article1Body">
                  {this.state.coinList.map(data=>{
                    let tradePrice = Number(data.krw.replace(/[^0-9\.]/g,''));
                    return (
                      <tr className="coinHref">
                        <td onClick={this.bitCoinHandleClick.bind(this, data)} >
                          <div>
                            <div class="miniChartUp" style={(data.rate >0)?{height:data.rate+"%"}:{height:"0%"}}>
                              <div>
                              </div>
                            </div>
                            <div class="miniChartDown" style={(data.rate <0)?{height:(-data.rate)+"%"}:{height:"0%"}}>
                              <div>
                              </div>
                            </div>
                          </div>
                          <div>
                            {(this.state.lang==='ko')? <p>{data.targetKo}</p> : <p>{data.targetEng}</p>}
                            <p>{data.target}/{data.source}</p>
                          </div>
                        </td>
                        <td style={data.rate>0? plusStyle: minusStyle} onClick={this.bitCoinHandleClick.bind(this, data)} ><span style={(this.state.active==="KRW")?{lineHeight:'27px'}:{lineHeight:"inherit"}}>{data.tradePrice}</span> <span>{(this.state.active==="KRW"&&tradePrice===0)?'':data.krw}</span></td>
                        <td style={data.rate>0? plusStyle: minusStyle} onClick={this.bitCoinHandleClick.bind(this, data)} >{data.rate}%</td>
                        <td onClick={this.bitCoinHandleClick.bind(this, data)} ><span>{data.accTradePrice24h}</span>{(this.state.active==="KRW")?"백만":''}</td>
                      </tr>
                    )}
                  )}    
                </tbody>
              </table>
            </div>
          </div>
        </article>
      </section>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Trade));