import React from 'react';
import { connect } from 'react-redux';
import { sendSingupEmailRequest} from '../actions/authentication';

const mapStateToProps = (state) => {
  return {
    isSignupEmail: state.authentication.email.isSignupEmail
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    sendSingupEmailRequest: (username) => {
      return dispatch(sendSingupEmailRequest(username));
    }
  };
};

class Passwordset extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      email: "",
    };
  }

  sendSignUpEmail()
  {
    this.props.sendSingupEmailRequest(this.state.email);
  }

  render()
  {
    return(
      <section className="pwSetCenter" id="pwSetCenter">
        <article id="pwSetArticle1">
          <div className="pwSetDiv">
            <p>패스워드 재설정</p>
            <p>이메일을 입력해 주십시오. 입력하신 이메일로 이메일 인증코드가 발송 됩니다.</p>
            <p><input type="text" placeholder="이메일 입력"  onChange={event=>this.setState({ email:event.target.value})}/></p>
            <p><a onClick={this.sendSignUpEmail.bind(this)}>이메일 인증코드 발송</a></p>
          </div>
        </article>
      </section>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Passwordset);